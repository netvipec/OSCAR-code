<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;O programie</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Release Notes</source>
        <translation>Uwagi do wydania</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>Zasługi</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>Licencja GPL</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="238"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="36"/>
        <source>Show data folder</source>
        <translation>Pokaż folder danych</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="88"/>
        <source>Sorry, could not locate About file.</source>
        <translation>Przepraszam, nie znaleziono pliku O programie.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="103"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>Przepraszam, nie znaleziono pliku Zasługi.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="133"/>
        <source>Important:</source>
        <translation>Ważne:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="147"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Aby sprawdzić, czy tekst licencji jest dostępny w Twoim języku, zobacz %1.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="118"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>Nie mogę znaleźć notatek o wydaniu.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="134"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation>Poniewaz jest to wersja rozwojowa, rekomendujemy &lt;b&gt;samodzielne zarchiwizowanie folderu danych&lt;/b&gt; przed kontynuacją , ponieważ próba przywrócenia poprzedniej wersji może się nie udać.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="40"/>
        <source>About OSCAR %1</source>
        <translation>O programie OSCAR %1</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="890"/>
        <source>Could not find the oximeter file:</source>
        <translation>Nie można znaleźć pliku pulsoksymetru:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="896"/>
        <source>Could not open the oximeter file:</source>
        <translation>Nie można otworzyć pliku pulsoksymetru:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="492"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Nie można uzyskać przekazu danych z pulsoksymetru.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="492"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translatorcomment>no</translatorcomment>
        <translation>Proszę sprawdzić czy wybrał &quot;wysyłaj&quot; w menu pulsoksymetru.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="560"/>
        <source>Could not find the oximeter file:</source>
        <translation>Nie można znaleźć pliku pulsoksymetru:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="566"/>
        <source>Could not open the oximeter file:</source>
        <translation>Nie można otworzyć pliku pulsoksymetru:</translation>
    </message>
</context>
<context>
    <name>CheckUpdates</name>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="250"/>
        <source>Checking for newer OSCAR versions</source>
        <translation>Sprawdzanie nowszych wersji OSCARa</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Idź do dnia poprzedzającego</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Pokaż/ukryj kalendarz</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Idź do następnego dnia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Idź do najpóźniejszego dnia z zapisanymi danymi</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>Pokaż rozmiar</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1391"/>
        <source>Notes</source>
        <translation>Notatki</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>dziennik</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1062"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1074"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1084"/>
        <source>Color</source>
        <translation>Kolor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1106"/>
        <location filename="../oscar/daily.ui" line="1116"/>
        <source>Small</source>
        <translation>Mały</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1121"/>
        <source>Medium</source>
        <translation>Średni</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Big</source>
        <translation>Duży</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1184"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1196"/>
        <source>I&apos;m feeling ...</source>
        <translation>Czuję ...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1212"/>
        <source>Weight</source>
        <translation>Waga</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1288"/>
        <source>Awesome</source>
        <translation>Super</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1326"/>
        <source>B.M.I.</source>
        <translation>B.M.I.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1342"/>
        <source>Bookmarks</source>
        <translation>Zakładki</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1363"/>
        <source>Add Bookmark</source>
        <translation>Dodaj zakładkę</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1386"/>
        <source>Starts</source>
        <translation>Rozpoczyna</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1399"/>
        <source>Remove Bookmark</source>
        <translation>Usuń zakładkę</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1407"/>
        <source>Search</source>
        <translation type="unfinished">Wyszukaj</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1495"/>
        <source>Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1502"/>
        <source>Save and Restore Graph Layout Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1515"/>
        <source>Show/hide available graphs.</source>
        <translation>Pokaż/ukryj dostępne wykresy.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="298"/>
        <source>Breakdown</source>
        <translation>Rozkład</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="298"/>
        <source>events</source>
        <translation>zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="310"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="311"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="370"/>
        <source>Time at Pressure</source>
        <translation>Czas z ciśnieniem</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="673"/>
        <source>No %1 events are recorded this day</source>
        <translation>Tego dnia zarejestrowano %1 zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="791"/>
        <source>%1 event</source>
        <translation>%1 zdarzenie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="792"/>
        <source>%1 events</source>
        <translation>%1 zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="835"/>
        <source>Session Start Times</source>
        <translation>Czas rozpoczęcia sesji</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="836"/>
        <source>Session End Times</source>
        <translation>Czas zakończenia sesji</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1032"/>
        <source>Session Information</source>
        <translation>Informacje o sesji</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1055"/>
        <source>Oximetry Sessions</source>
        <translation>Sesje z pulsoksymetrem</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1075"/>
        <source>Duration</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1052"/>
        <source>CPAP Sessions</source>
        <translation>Sesje CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1058"/>
        <source>Sleep Stage Sessions</source>
        <translation>Sesje faz snu</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1061"/>
        <source>Position Sensor Sessions</source>
        <translation>Sesje z czujnikiem pozycji</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>Unknown Session</source>
        <translation>Nieznana sesja</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1303"/>
        <source>Model %1 - %2</source>
        <translation>Model %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1308"/>
        <source>PAP Mode: %1</source>
        <translation>Tryb PAP- %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1429"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Ten dzień zawiera tylko dane sumaryczne, jest dostępna tylko ograniczona informacja.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1455"/>
        <source>Total ramp time</source>
        <translation>Całkowity czas rampy</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1459"/>
        <source>Time outside of ramp</source>
        <translation>Czas poza rampą</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1500"/>
        <source>Start</source>
        <translation>Początek</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1500"/>
        <source>End</source>
        <translation>Koniec</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1537"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>Nie można pokazać wykresu kołowego w tym systemie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1827"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Tu nic nie ma!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1276"/>
        <source>Oximeter Information</source>
        <translation>Informacje pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="190"/>
        <source>Details</source>
        <translation>Szczegóły</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="584"/>
        <source>Disable Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="585"/>
        <source>Disabling a session will remove this session data 
from all  graphs, reports and statistics.

The Search tab can find disabled sessions

Continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>Click to %1 this session.</source>
        <translation>Kliknij aby %1 tę sesję.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>disable</source>
        <translation>wyłączyć</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1095"/>
        <source>enable</source>
        <translation>włączyć</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1111"/>
        <source>%1 Session #%2</source>
        <translation>%1 sesja #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1112"/>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1143"/>
        <source>Device Settings</source>
        <translation>Ustawienia aparatu</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1281"/>
        <source>SpO2 Desaturations</source>
        <translation>Desaturacje SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1282"/>
        <source>Pulse Change events</source>
        <translation>Zdarzenia zmiany pulsu</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1283"/>
        <source>SpO2 Baseline Used</source>
        <translation>Użyta linia podstawowa SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1349"/>
        <source>Statistics</source>
        <translation>Statystyki</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1439"/>
        <source>Total time in apnea</source>
        <translation>Całkowity czas bezdechu</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1449"/>
        <source>Time over leak redline</source>
        <translation>Czas powyżej ostrzegawczej linii wycieku</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1524"/>
        <source>Event Breakdown</source>
        <translation>Rozkład zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1727"/>
        <source>This CPAP device does NOT record detailed data</source>
        <translation>To urządzenie CPAP NIE rejestruje szczegółowych danych</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1798"/>
        <source>Sessions all off!</source>
        <translation>Wszystkie sesje wyłączone!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1800"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Są sesje dla tego dnia, ale są wyłączone.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1803"/>
        <source>Impossibly short session</source>
        <translation>Niemożliwie krótka sesja</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1804"/>
        <source>Zero hours??</source>
        <translation>Zero godzin??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1809"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Poskarż się sprzedawcy sprzętu!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2129"/>
        <source>Pick a Colour</source>
        <translation>Wybierz kolor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2466"/>
        <source>Bookmark at %1</source>
        <translation>Zrób zakładkę przy %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1830"/>
        <source>No data is available for this day.</source>
        <translation>Brak danych dla tego dnia.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1219"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation>Jeśli w preferencjach wzrost jest powyżej zera, podanie wagi spowoduje wyliczenie BMI</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1146"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation>&lt;b&gt;Uwaga&lt;/b&gt; Wszystkie ustawienia podane ponizej są oparte na założeniu, że nic się nie zmieniło w poprzedzających dniach.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1807"/>
        <source>no data :(</source>
        <translation>Brak danych</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1808"/>
        <source>Sorry, this device only provides compliance data.</source>
        <translation>Niestety ten aparat dostarcza tylko danych o zgodności.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2423"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation>Ta zakładka nie jest aktuallnie obsługiwana.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1312"/>
        <source>(Mode and Pressure settings missing; yesterday&apos;s shown.)</source>
        <translation>(Brak ustawień trybu i ciśnienia - pokazuję wczorajsze.)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="148"/>
        <source>Hide All Events</source>
        <translation type="unfinished">Ukryj wszystkie zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="149"/>
        <source>Show All Events</source>
        <translation type="unfinished">Pokaż wszystkie zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="150"/>
        <source>Hide All Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.h" line="151"/>
        <source>Show All Graphs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DailySearchTab</name>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="180"/>
        <source>Match:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="192"/>
        <location filename="../oscar/dailySearchTab.cpp" line="985"/>
        <source>Select Match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="227"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="228"/>
        <location filename="../oscar/dailySearchTab.cpp" line="977"/>
        <location filename="../oscar/dailySearchTab.cpp" line="1087"/>
        <source>Start Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="250"/>
        <source>DATE
Jumps to Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="262"/>
        <source>Notes</source>
        <translation type="unfinished">Notatki</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="263"/>
        <source>Notes containing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="264"/>
        <source>Bookmarks</source>
        <translation type="unfinished">Zakładki</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="265"/>
        <source>Bookmarks containing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="266"/>
        <source>AHI </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="267"/>
        <source>Daily Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="268"/>
        <source>Session Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="269"/>
        <source>Days Skipped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="270"/>
        <source>Disabled Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="271"/>
        <source>Number of Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="318"/>
        <source>Click HERE to close Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="322"/>
        <source>Help</source>
        <translation type="unfinished">Pomoc</translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="526"/>
        <source>No Data
Jumps to Date&apos;s Details </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="531"/>
        <source>Number Disabled Session
Jumps to Date&apos;s Details </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="537"/>
        <location filename="../oscar/dailySearchTab.cpp" line="554"/>
        <source>Note
Jumps to Date&apos;s Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="542"/>
        <location filename="../oscar/dailySearchTab.cpp" line="547"/>
        <source>Jumps to Date&apos;s Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="561"/>
        <source>AHI
Jumps to Date&apos;s Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="567"/>
        <source>Session Duration
Jumps to Date&apos;s Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="574"/>
        <source>Number of Sessions
Jumps to Date&apos;s Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="581"/>
        <source>Daily Duration
Jumps to Date&apos;s Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="589"/>
        <source>Number of events
Jumps to Date&apos;s Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="599"/>
        <source>Automatic start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="819"/>
        <source>More to Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="822"/>
        <source>Continue Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="825"/>
        <source>End of Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="830"/>
        <source>No Matches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1035"/>
        <source> Skip:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1036"/>
        <source>%1/%2%3 days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1127"/>
        <source>Finds days that match specified criteria.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1129"/>
        <source>  Searches from last day to first day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1132"/>
        <source>First click on Match Button then select topic.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1134"/>
        <source>  Then click on the operation to modify it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1136"/>
        <source>  or update the value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1138"/>
        <source>Topics without operations will automatically start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1141"/>
        <source>Compare Operations: numberic or character. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1143"/>
        <source>  Numberic  Operations: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1146"/>
        <source>  Character Operations: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1150"/>
        <source>Summary Line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1152"/>
        <source>  Left:Summary - Number of Day searched</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1154"/>
        <source>  Center:Number of Items Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1156"/>
        <source>  Right:Minimum/Maximum for item searched</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1158"/>
        <source>Result Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1160"/>
        <source>  Column One: Date of match. Click selects date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1162"/>
        <source>  Column two: Information. Click selects date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1164"/>
        <source>    Then Jumps the appropiate tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1167"/>
        <source>Wildcard Pattern Matching:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1170"/>
        <source>  Wildcards use 3 characters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1172"/>
        <source>  Asterisk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1175"/>
        <source> Question Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1178"/>
        <source> Backslash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1181"/>
        <source>  Asterisk matches any number of characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1183"/>
        <source>  Question Mark matches a single character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1185"/>
        <source>  Backslash matches next character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/dailySearchTab.cpp" line="1039"/>
        <source>Found %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DateErrorDisplay</name>
    <message>
        <location filename="../oscar/overview.cpp" line="814"/>
        <source>ERROR
The start date MUST be before the end date</source>
        <translation>BŁĄD
Data rozpoczęcia MUSI przypadać przed datą zakończenia</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="817"/>
        <source>The entered start date %1 is after the end date %2</source>
        <translation>Wprowadzona data rozpoczęcia %1 jest późniejsza niż data zakończenia %2</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="818"/>
        <source>
Hint: Change the end date first</source>
        <translation>
Wskazówka: najpierw zmień datę zakończenia</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="820"/>
        <source>The entered end date %1 </source>
        <translation>Wprowadzona data zakończenia %1 </translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="821"/>
        <source>is before the start date %1</source>
        <translation>jest przed datą początkową %1</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="822"/>
        <source>
Hint: Change the start date first</source>
        <translation>
Wskazówka: najpierw zmień datę rozpoczęcia</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Eksportuj jako CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Daty:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Rozdzielczość:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Szczegóły</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Sesje</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Dziennie</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Nazwa pliku:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Skasuj</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Wyeksportuj</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Początek:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Koniec:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Szybki zakres:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>Najnowszy dzień</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>Ostatni tydzień</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translation>Ostatnie dwa tygodnie</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation>Ostatni miesiąc</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>Ostatnie 6 miesięcy</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>Ostatni rok</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>Wszystko</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>Wybór własny</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translation>Szczegóły_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translation>Sesje_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translation>Podsumowanie_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translation>Wybór pliku do eksportu</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>Pliki CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>Data i czas</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>Sesja</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>Zdarzenie</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>Data/Czas trwania</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>Ilość sesji</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>Początek</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>Koniec</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>Czas całkowity</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> Ilość</translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="242"/>
        <source>Import Error</source>
        <translation>Błąd importu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="243"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation>Ten zapis z aparatu nie moze być zaimportowany do tego profilu.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="243"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Zapis z dnia nakłada się na istniejący zapis.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="91"/>
        <source>Hide this message</source>
        <translation>Ukryj tę wiadomość</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="196"/>
        <source>Search Topic:</source>
        <translation>Temat wyszukiwania:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="59"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Pliki pomocy dla %1 nie są dotąd dostępne i będą wyświetlone w %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="69"/>
        <source>Help files do not appear to be present.</source>
        <translation>Nie ma plików pomocy.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="85"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>Pomoc nie jest prawidłowo ustawiona</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="100"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>Pomoc nie może prawidłowo zarejestrować dokumentacji.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Contents</source>
        <translation>Zawartość</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="112"/>
        <source>Index</source>
        <translation>Indeks</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="116"/>
        <source>Search</source>
        <translation>Wyszukaj</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="126"/>
        <source>No documentation available</source>
        <translation>Brak dostępnej dokumentacji</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="214"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translatorcomment>Poczekaj, indeksowanie w trakcie</translatorcomment>
        <translation>Poczekaj, indeksowanie w trakcie</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 wynik(ów) dla &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="242"/>
        <source>clear</source>
        <translation>czysty</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="166"/>
        <source>Could not find the oximeter file:</source>
        <translation>Nie można znaleźć pliku pulsoksymetru:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="172"/>
        <source>Could not open the oximeter file:</source>
        <translatorcomment>Nie można otworzyć pliku pulsoksymetru:</translatorcomment>
        <translation>Nie można otworzyć pliku pulsoksymetru:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="507"/>
        <source>&amp;Statistics</source>
        <translation>&amp;Statystyki</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="561"/>
        <source>Report Mode</source>
        <translation>Tryb raportu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="568"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="578"/>
        <source>Monthly</source>
        <translation>Miesięcznie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="585"/>
        <source>Date Range</source>
        <translation>Zakres dat</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="956"/>
        <source>Statistics</source>
        <translation>Statystyki</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1006"/>
        <source>Daily</source>
        <translation>Dziennie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1050"/>
        <source>Overview</source>
        <translation>Przegląd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1094"/>
        <source>Oximetry</source>
        <translation>Pulsoksymetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1144"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1194"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2345"/>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2370"/>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2399"/>
        <source>&amp;Help</source>
        <translation>&amp;Pomoc</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2429"/>
        <source>&amp;Data</source>
        <translation>&amp;Dane</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2433"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Zaawansowane</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2443"/>
        <source>Purge ALL Device Data</source>
        <translation>Wyczyść wszystkie dane aparatu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2811"/>
        <source>Report an Issue</source>
        <translation>Zgłoś problem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2465"/>
        <source>Rebuild CPAP Data</source>
        <translation>Przebuduj dane CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2496"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Preferencje</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2501"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Profile</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2781"/>
        <source>Show Performance Information</source>
        <translation>Pokaż informację o wydajności</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2801"/>
        <source>CSV Export Wizard</source>
        <translation>Kreator ekportu CSV</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2806"/>
        <source>Export for Review</source>
        <translation>Eksport do przeglądu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="119"/>
        <source>E&amp;xit</source>
        <translation>W&amp;yjście</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2506"/>
        <source>Exit</source>
        <translation>Wyjście</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2511"/>
        <source>View &amp;Daily</source>
        <translation>Widok &amp;Dziennie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2522"/>
        <source>View &amp;Overview</source>
        <translation>Widok &amp;Przegląd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2533"/>
        <source>View &amp;Welcome</source>
        <translation>Widok &amp;Witaj</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2558"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Użyj &amp;AntiAliasing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2585"/>
        <source>Show Debug Pane</source>
        <translation>Pokaż okno debugowania</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2601"/>
        <source>Take &amp;Screenshot</source>
        <translation>Zrób &amp;Zrzut ekranu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2609"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>Kreator Pulso&amp;xymetru</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2617"/>
        <source>Print &amp;Report</source>
        <translation>Drukuj &amp;Raport</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2622"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Edytuj profil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2762"/>
        <source>Daily Calendar</source>
        <translation>Kalendarz Dzienny</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2773"/>
        <source>Backup &amp;Journal</source>
        <translation>Zapisz &amp;Dziennik</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2627"/>
        <source>Online Users &amp;Guide</source>
        <translation>&amp;Przewodnik użytkownika online</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="912"/>
        <source>Profiles</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2437"/>
        <source>Purge Oximetry Data</source>
        <translation>Wyczyść dane pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2563"/>
        <source>&amp;About OSCAR</source>
        <translation>&amp;O programie OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2632"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>&amp;Często zadawane pytania</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2637"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>&amp;Automatyczne czyszczenie danych pulsoksymetrii</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2642"/>
        <source>Change &amp;User</source>
        <translation>Zmień &amp;Użytkownika</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2448"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Wyczyść &amp;wybrany dzień</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2653"/>
        <source>Right &amp;Sidebar</source>
        <translation>Prawy &amp;pasek boczny</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2745"/>
        <source>Daily Sidebar</source>
        <translation>Dzienny pasek boczny</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2664"/>
        <source>View S&amp;tatistics</source>
        <translation>Pokaż s&amp;tatystyki</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="885"/>
        <source>Navigation</source>
        <translation>Nawigacja</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1316"/>
        <source>Bookmarks</source>
        <translation>Zakładki</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2285"/>
        <source>Records</source>
        <translation>Zapisy</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2349"/>
        <source>Exp&amp;ort Data</source>
        <translation>Wyeksp&amp;ortuj dane</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2667"/>
        <source>View Statistics</source>
        <translation>Pokaż statystyki</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2678"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Importuj dane &amp;ZEO</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2688"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Importuj dane RemStar &amp;MSeries</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2693"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>Terminologia zaburzeń snu &amp;Słownik</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2698"/>
        <source>Change &amp;Language</source>
        <translation>Zmień &amp;Język</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2703"/>
        <source>Change &amp;Data Folder</source>
        <translation>Zmień folder &amp;danych</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2708"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Importuj dane pozycji &amp;snu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2718"/>
        <source>Current Days</source>
        <translation>Bieżące dni</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="560"/>
        <location filename="../oscar/mainwindow.cpp" line="2227"/>
        <source>Welcome</source>
        <translation>Witaj</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="116"/>
        <source>&amp;About</source>
        <translation>&amp;O programie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="830"/>
        <location filename="../oscar/mainwindow.cpp" line="1903"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Proszę czekać, importuję z kopii zapasowej folderu (ów)...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="766"/>
        <source>Import Problem</source>
        <translation>Problem z importem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="931"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Proszę włóż kartę danych CPAP...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1021"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Dostęp do importu został zablokowany podczas trwających przeliczeń.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1073"/>
        <source>CPAP Data Located</source>
        <translation>Zlokalizowano dane CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1108"/>
        <source>Import Reminder</source>
        <translation>Przypomnienie o imporcie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1189"/>
        <source>Importing Data</source>
        <translation>Importuję dane</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="288"/>
        <source>Help Browser</source>
        <translation>Przeglądarka pomocy</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="542"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Ładowanie profilu:&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1025"/>
        <source>Import is already running in the background.</source>
        <translation>Import działa w tle.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1380"/>
        <source>Please open a profile first.</source>
        <translation>Proszę najpierw otworzyć profil.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1573"/>
        <source>The FAQ is not yet implemented</source>
        <translation>FAQ nie działa</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1715"/>
        <location filename="../oscar/mainwindow.cpp" line="1742"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>Jeżeli to widać, restart się nie powiódł. Trzeba to zrobić ręcznie.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2589"/>
        <source>You must select and open the profile you wish to modify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2624"/>
        <source>Export review is not yet implemented</source>
        <translation>Podgląd eksportu chwilowo niedostępny</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2834"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>Zgłaszanie problemów chwilowo niedostępne</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2602"/>
        <source>%1&apos;s Journal</source>
        <translation>Dziennik %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2604"/>
        <source>Choose where to save journal</source>
        <translation>Wybierz, gdzie zapisać dziennik</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2604"/>
        <source>XML Files (*.xml)</source>
        <translation>Pliki XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1890"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Jeżeli utworzyłeś &lt;i&gt;swoje&lt;b&gt;własne&lt;/b&gt;kopie zapasowe WSZYSTKICH swoich danych CPAP&lt;/i&gt; nadal możesz wykonać tą czynność, ale będziesz musiał ręcznie przywrócić dane.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1891"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Naprawdę chcesz to zrobić?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1906"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Ponieważ brak wewnętrznej kopii zapasowej, musisz użyć własnej do odbudowy.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1956"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Uwaga, folder kopii zapasowych pozostanie na swoim miejscu.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1968"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Jesteś &lt;b&gt;absolutnie pewny&lt;/b&gt;, że chcesz to zrobić?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2073"/>
        <source>No help is available.</source>
        <translation>Brak pomocy.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2510"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Jesteś pewny, że chcesz usunąć dane pulsoksymetrii dla %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2512"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;UWAGA! Tej operacji nie da się cofnąć!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2542"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Najpierw należy wybrać dzień z ważnymi danymi pulsoksymetrii w widoku dziennym.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="762"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>Zaimportowano %1 sesji CPAP

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="762"/>
        <source>Import Success</source>
        <translation>Import zakończony pomyślnie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="764"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>Aktualne z danymi CPAP na

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="764"/>
        <source>Up to date</source>
        <translation>Aktualne</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="938"/>
        <source>Choose a folder</source>
        <translation>Wybierz folder</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1066"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>Struktura plików %1 dla %2 jest zlokalizowana w:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1068"/>
        <source>A %1 file structure was located at:</source>
        <translation>Struktura plików %1 została zlokalizowana w:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1033"/>
        <source>Would you like to import from this location?</source>
        <translation>Czy chcesz importować z tej lokalizacji?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="766"/>
        <source>Couldn&apos;t find any valid Device Data at

%1</source>
        <translation>Nie mogę znaleźć odpowiednich danych w

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1077"/>
        <source>Specify</source>
        <translation>Sprecyzuj</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1168"/>
        <source>No supported data was found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1385"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Dostęp do preferencji został zablokowany do czasu zakończenia obliczeń.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1501"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Błąd przy zapisywaniu zrzutu ekranu do pliku &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1503"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Zrzut ekranu zapisany do pliku &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1878"/>
        <source>Are you sure you want to rebuild all CPAP data for the following device:

</source>
        <translation>Na pewno chcesz przebudować wszystkie dane dla :

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1888"/>
        <source>For some reason, OSCAR does not have any backups for the following device:</source>
        <translation>Z pewnych powodów OSCAR nie ma żadnych kopii zapasowych dla:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1907"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this device until you do)</source>
        <translation>Czy chcesz importować z własnych kopii zapasowych teraz? (nie będziesz widział żadnych danych z tego aparatu dopóki nie importujesz)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1958"/>
        <source>OSCAR does not have any backups for this device!</source>
        <translation>OSCAR nie ma żadnych plików zapasowych dla tego aparatu!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1959"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this device&lt;/i&gt;, &lt;font size=+2&gt;you will lose this device&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation>Dopóki nie utworzysz &lt;i&gt;swoich &lt;b&gt;własnych &lt;/b&gt; kopii zapasowych WSZYSTKICH danych dla tego aparatu&lt;/i&gt;, &lt;font size=+2&gt;utracisz wszwszystkie dane tego aparatu &lt;b&gt;trwale&lt;/b&gt;!&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1965"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s device database for the following device:&lt;/p&gt;</source>
        <translation>Usuwasz &lt;font size=+2&gt;bezpowrotnie&lt;/font&gt; bazę danych dla aparatu:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2023"/>
        <source>A file permission error caused the purge process to fail; you will have to delete the following folder manually:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2271"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Problem z otwarciem pliku blokującego MSeries: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2275"/>
        <source>MSeries Import complete</source>
        <translation>Ukończono import MSeries</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1881"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>Gdy kopie zapasowe są wyłączone lub zaburzone w inny sposób, może to spowodować utratę danych.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1017"/>
        <source>No profile has been selected for Import.</source>
        <translation>Nie wybrano profilu do importu.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2571"/>
        <source>&amp;Maximize Toggle</source>
        <translation>&amp;Przełącznik Maksymalizowania</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1565"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>Podręcznik użytkownika otworzy się w oknie domyślnej przeglądarki</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2286"/>
        <source>The Glossary will open in your default browser</source>
        <translation>Słownik otworzy się w oknie domyślnej przeglądarki</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2514"/>
        <source>Show Daily view</source>
        <translation>Pokaż widok dzienny</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2525"/>
        <source>Show Overview view</source>
        <translation>Pokaż Przegląd</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2574"/>
        <source>Maximize window</source>
        <translation>Maksymalizuj okno</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2596"/>
        <source>Reset sizes of graphs</source>
        <translation>Zresetuj wielkość wykresów</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2656"/>
        <source>Show Right Sidebar</source>
        <translation>Pokaż prawy pasek boczny</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2670"/>
        <source>Show Statistics view</source>
        <translation>Pokaż Statystyki</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2726"/>
        <source>Show &amp;Line Cursor</source>
        <translation>Pokaż kursor &amp;linii</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2748"/>
        <source>Show Daily Left Sidebar</source>
        <translation>Pokaż lewy pasek boczny dla widoku dziennego</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2765"/>
        <source>Show Daily Calendar</source>
        <translation>Pokaż kalendarz</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2816"/>
        <source>System Information</source>
        <translation>Informacje systemowe</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2827"/>
        <source>Show &amp;Pie Chart</source>
        <translation>Pokaż wykres &amp;ciasteczkowy</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>Show Pie Chart on Daily page</source>
        <translation>Pokaż wykres ciasteczkowy w widoku dziennym</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1168"/>
        <location filename="../oscar/mainwindow.cpp" line="2843"/>
        <source>OSCAR Information</source>
        <translation>OSCAR - Informacje</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2374"/>
        <source>&amp;Reset Graphs</source>
        <translation>&amp;Resetuj wykresy</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2593"/>
        <source>Reset Graph &amp;Heights</source>
        <translation>Resetuj wykresy i &amp;wysokości</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2403"/>
        <source>Troubleshooting</source>
        <translation>Rozwiązywanie problemów</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2488"/>
        <source>&amp;Import CPAP Card Data</source>
        <translation>&amp;Importuj dane karty CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2683"/>
        <source>Import &amp;Dreem Data</source>
        <translation>Importuj dane &amp;Dreem</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2786"/>
        <source>Create zip of CPAP data card</source>
        <translation>Utwórz archiwum zip danych karty CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2796"/>
        <source>Create zip of all OSCAR data</source>
        <translation>Utwórz archiwum zip wszystkich danych OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="581"/>
        <source>%1 (Profile: %2)</source>
        <translation>%1 (Profile: %2)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1107"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation>Proszę, pamiętaj wybrać folder podstawowy lub literę dysku karty, a nie folderu wewnętrznego.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1491"/>
        <source>Choose where to save screenshot</source>
        <translation>Wybierz, gdzie zapisać zrzut ekranu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1491"/>
        <source>Image files (*.png)</source>
        <translation>Pliki obrazu (*.png)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2634"/>
        <source>Would you like to zip this card?</source>
        <translation>Czy chcesz zarchiwizować tę kartę?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2656"/>
        <location filename="../oscar/mainwindow.cpp" line="2727"/>
        <location filename="../oscar/mainwindow.cpp" line="2778"/>
        <source>Choose where to save zip</source>
        <translation>Wybierz, gdzie zapisać zip</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2656"/>
        <location filename="../oscar/mainwindow.cpp" line="2727"/>
        <location filename="../oscar/mainwindow.cpp" line="2778"/>
        <source>ZIP files (*.zip)</source>
        <translation>Pliki ZIP (*.zip)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2703"/>
        <location filename="../oscar/mainwindow.cpp" line="2741"/>
        <location filename="../oscar/mainwindow.cpp" line="2812"/>
        <source>Creating zip...</source>
        <translation>Tworzenie archiwum...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2688"/>
        <location filename="../oscar/mainwindow.cpp" line="2796"/>
        <source>Calculating size...</source>
        <translation>Obliczanie rozmiaru...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2860"/>
        <source>Show Personal Data</source>
        <translation>Pokaż dane osobiste</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2791"/>
        <source>Create zip of OSCAR diagnostic logs</source>
        <translation>Utwórz zip logów diagnostycznych OSCARa</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2868"/>
        <source>Check For &amp;Updates</source>
        <translation>Sprawdź &amp;Uaktualnienia</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1442"/>
        <source>Check for updates not implemented</source>
        <translation>Sprawdzanie uaktualnień nie wprowadzone</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2713"/>
        <source>Import &amp;Viatom/Wellue Data</source>
        <translation>Import danych &amp;Viatom/Wellue</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2838"/>
        <source>Standard - CPAP, APAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2841"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Standard graph order, good for CPAP, APAP,  Basic BPAP&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2846"/>
        <source>Advanced - BPAP, ASV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2849"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Advanced graph order, good for BPAP w/BU, ASV, AVAPS, IVAPS&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2876"/>
        <source>Purge Current Selected Day</source>
        <translation>Wyczyść aktualnie wybrany dzień</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2881"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2886"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Pulsoksymetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2891"/>
        <source>&amp;Sleep Stage</source>
        <translation>Faza &amp;Snu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2896"/>
        <source>&amp;Position</source>
        <translation>&amp;Pozycja</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2901"/>
        <source>&amp;All except Notes</source>
        <translation>&amp;wszystko oprócz notatek</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2906"/>
        <source>All including &amp;Notes</source>
        <translation>Wszystko włącznie z &amp;notatkami</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1123"/>
        <source>Find your CPAP data card</source>
        <translation>Znajdź kartę danych CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2374"/>
        <location filename="../oscar/mainwindow.cpp" line="2378"/>
        <source>There was a problem opening %1 Data File: %2</source>
        <translation>Był problem z otwarciem %1 pliku danych:%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2377"/>
        <source>%1 Data Import of %2 file(s) complete</source>
        <translation>%1 import danych z %2 plik(ów zakończony</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2379"/>
        <source>%1 Import Partial Success</source>
        <translation>%1 częściowe powodzenie importu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2381"/>
        <source>%1 Data Import complete</source>
        <translation>%1 import zakończony</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2186"/>
        <source>Auto-Fit</source>
        <translation>Autodopasowanie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2187"/>
        <source>Defaults</source>
        <translation>Domyślne</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2188"/>
        <source>Override</source>
        <translation>Nadpisanie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2189"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Tryb skalowania osi Y. &quot;Autodopasowanie&quot; dla skalowania automatycznego, &quot;Domyślne&quot; dla ustawień odpowiednio dla producenta, &quot;Nadpisanie&quot; dla wyboru własnego.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2195"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Minimalna wartość osi Y. Może być liczbą ujemną jeśli tak chcesz.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2196"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Maksymalna wartość osi Y. Musi być większa od minimalnej.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2231"/>
        <source>Scaling Mode</source>
        <translation>Tryb skalowania</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2253"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Ten przycisk resetuje Min i Max do Autodopasowania</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Edytuj profil użytkownika</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="73"/>
        <source>I agree to all the conditions above.</source>
        <translation>Zgadzam się na wszystkie powyższe warunki.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="114"/>
        <source>User Information</source>
        <translation>Informacje o użytkowniku</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="140"/>
        <source>User Name</source>
        <translation>Nazwa użytkownika</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="158"/>
        <source>Password Protect Profile</source>
        <translation>Profil chroniony hasłem</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="185"/>
        <source>Password</source>
        <translation>Hasło</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="199"/>
        <source>...twice...</source>
        <translation>...dwukrotnie...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="216"/>
        <source>Locale Settings</source>
        <translation>Ustawienia lokalizacji</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="288"/>
        <source>Country</source>
        <translation>Kraj</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="256"/>
        <source>TimeZone</source>
        <translation>Strefa czasowa</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="243"/>
        <source>DST Zone</source>
        <translation>Strefa czasu letniego</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="323"/>
        <source>Personal Information (for reports)</source>
        <translation>Informacje osobiste (do raportów)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="347"/>
        <source>First Name</source>
        <translation>Imię</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="357"/>
        <source>Last Name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Oczywiście można to pominąć, ale przybliżony wiek jest potrzebny do precyzyjniejszych obliczeń.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="370"/>
        <source>D.O.B.</source>
        <translation>Data urodzenia.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Płeć biologiczna jest czasem potrzebna do precyzyjniejszych obliczeń, ale można to pominąć.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="389"/>
        <source>Gender</source>
        <translation>Płeć</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="402"/>
        <source>Male</source>
        <translation>Męska</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="407"/>
        <source>Female</source>
        <translation>Żeńska</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="423"/>
        <source>Height</source>
        <translation>Wzrost</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="480"/>
        <source>Contact Information</source>
        <translation>Informacje kontaktowe</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="507"/>
        <location filename="../oscar/newprofile.ui" line="782"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="524"/>
        <location filename="../oscar/newprofile.ui" line="813"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="534"/>
        <location filename="../oscar/newprofile.ui" line="803"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="579"/>
        <source>CPAP Treatment Information</source>
        <translation>Leczenie CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="606"/>
        <source>Date Diagnosed</source>
        <translation>Data diagnozy</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="620"/>
        <source>Untreated AHI</source>
        <translation>AHI bez leczenia</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="634"/>
        <source>CPAP Mode</source>
        <translation>Tryb CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="642"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="647"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="652"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="657"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="665"/>
        <source>RX Pressure</source>
        <translation>Ciśnienie RX</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="711"/>
        <source>Doctors / Clinic Information</source>
        <translation>Informacje o lekarzu/klinice</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="738"/>
        <source>Doctors Name</source>
        <translation>Nazwisko lekarza</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="755"/>
        <source>Practice Name</source>
        <translation>Nazwa praktyki</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="765"/>
        <source>Patient ID</source>
        <translation>ID pacjenta</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="957"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Skasuj</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="973"/>
        <source>&amp;Back</source>
        <translation>&amp;Wstecz</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="989"/>
        <location filename="../oscar/newprofile.cpp" line="279"/>
        <location filename="../oscar/newprofile.cpp" line="288"/>
        <source>&amp;Next</source>
        <translation>&amp;Następny</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="63"/>
        <source>Select Country</source>
        <translation>Wybierz kraj</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>PROSZĘ UWAŻNIE PRZECZYTAĆ</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>Dokładność przedstawionych danych nie jest gwarantowana.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Jakiekolwiek raporty są przeznaczone do UŻYTKU OSOBISTEGO i W ŻADNYM WYPADKU nie służą do celów diagnostyki medycznej.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="131"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>Używasz tego programu wyłącznie na własne ryzyko.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR jest udostępniany jako wolne oprogramowanie pod &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, i nie daje gwarancji ani praw do roszczeń z jakiegokolwiek powodu.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="113"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP Devices and related equipment.</source>
        <translation>Ten program jest przeznaczony do pomocy w ocenie danych z aparatu CPAP i akcesoriów.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="119"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR ma służyć pomocą w przeglądaniu danych CPAP, ale nie zastępowaniu pomocy lekarskiej.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>Autorzy nie ponoszą odpowiedzialności za cokolwiek mającego związek z używaniem oprogramowania.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="157"/>
        <source>Please provide a username for this profile</source>
        <translation>Proszę podać nazwę użytkownika dla tego profilu</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="167"/>
        <source>Passwords don&apos;t match</source>
        <translation>Hasła nie są jednakowe</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Profile Changes</source>
        <translation>Zmiany profilu</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Accept and save this information?</source>
        <translation>Czy zaakceptować i zapisać informacje?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="277"/>
        <source>&amp;Finish</source>
        <translation>&amp;Zakończ</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="455"/>
        <source>&amp;Close this window</source>
        <translation>&amp;Zamknij okno</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Witaj w OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="462"/>
        <source>Metric</source>
        <translation>metryczny</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="467"/>
        <source>English</source>
        <translation>angielski</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="134"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019-2022 The OSCAR Team</source>
        <translation>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019-2022 The OSCAR Team</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Very weak password protection and not recommended if security is required.</source>
        <translation>Bardzo słaba ochrona hasłem, nie rekomendowana jeżeli wymagane jest bezpieczeństwo.</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Zakres:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Ostatni tydzień</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Ostatnie 2 tygodnie</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Ostatni miesiąc</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Ostatnie dwa miesiące</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Ostatnie trzy miesiące</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Ostatnie 6 miesięcy</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Ostatni rok</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Wszystko</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Własne</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="135"/>
        <source>Start:</source>
        <translation>Początek:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="158"/>
        <source>End:</source>
        <translation>Koniec:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="181"/>
        <source>Reset view to selected date range</source>
        <translation>Zresetuj widok do wybranego zakresu dat</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="230"/>
        <source>Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="237"/>
        <source>Save and Restore Graph Layout Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Pokaż listę wykresów do włączenia/wyłączenia.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="251"/>
        <source>Graphs</source>
        <translation>Wykresy</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="267"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Wskaźnik
Zaburzeń
Oddechowych</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="269"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Wskaźnik
Niedotlenienie
Bezdech</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="276"/>
        <source>Usage</source>
        <translation>Użycie</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="276"/>
        <source>Usage
(hours)</source>
        <translation>Użycie
(godziny)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="281"/>
        <source>Session Times</source>
        <translation>Czasy sesji</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="290"/>
        <source>Total Time in Apnea</source>
        <translation>Całkowity czas bezdechu</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="290"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Całkowity czas bezdechu
(minuty)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="347"/>
        <source>Body
Mass
Index</source>
        <translation>Indeks
Masy
Ciała (BMI)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="353"/>
        <source>How you felt
(0-10)</source>
        <translation>Jak się czułeś
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="127"/>
        <source>Snapshot</source>
        <translation>migawka</translation>
    </message>
    <message>
        <location filename="../oscar/overview.h" line="202"/>
        <source>Hide All Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.h" line="203"/>
        <source>Show All Graphs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="39"/>
        <source>Oximeter Import Wizard</source>
        <translation>Kreator importu pulsoksymetrii</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="444"/>
        <source>Skip this page next time.</source>
        <translation>Następnym razem pomiń tę stronę.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="499"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;First select your correct oximeter type from the pull-down menu below.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Uwaga: &lt;/span&gt;&lt;span style=&quot; font-style:italic; &quot;&gt;Najpierw wybierz właściwy typ pulsoksymetru z menu rozwijanego poniżej.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="542"/>
        <source>Where would you like to import from?</source>
        <translation>Skąd chcesz importować?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="586"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;FIRST Select your Oximeter from these groups:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;NAJPIERW Wybierz swój pulsoksymetr z tych grup:&lt;/span&gt;&lt;/p&gt;&lt;/ body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="648"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>Użytkownicy CMS50E/F, jeśli importujecie bezpośrednio, nie wybierajcie wysyłki zanim OSCAR poprosi o to.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="685"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeżeli jest to włączone, OSCAR automatycznie zresetuje zegar wewnętrzny CMS50 używając bieżącego czasu komputera.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="829"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Jeśli nie przeszkadza Ci bycie podłączonym do włączonego komputera całą noc, ta opcja dostarczy wykresu pletyzmograficznego, który pokaże rytm pracy serca powyżej normalnych wskazań SpO2. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="835"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Zapis podłączenia do komputera przez noc(dostarcza pletyzmogram)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="868"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ta opcja pozwala na importowanie z plików danych utworzonych przez oprogramowanie pulsoksymetru, jak np. SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="874"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Importuj z plików danych zachowanych przez inny program, np. SpO2Review</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="953"/>
        <source>Please connect your oximeter device</source>
        <translation>Proszę podłączyć pulsoksymetr</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="971"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Jeśli można to przeczytać, prawdopodobnie źle ustawiono typ pulsoksymetru w preferencjach.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1026"/>
        <source>Press Start to commence recording</source>
        <translation>Naciśnij Start aby rozpocząć zapis</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1071"/>
        <source>Show Live Graphs</source>
        <translation>Pokaż wykresy</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1101"/>
        <location filename="../oscar/oximeterimport.ui" line="1353"/>
        <source>Duration</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1217"/>
        <source>Pulse Rate</source>
        <translation>Częstość pulsu</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1309"/>
        <source>Multiple Sessions Detected</source>
        <translation>Wykryto wiele sesji</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1348"/>
        <source>Start Time</source>
        <translation>Czas rozpoczęcia</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1358"/>
        <source>Details</source>
        <translation>Szczegóły</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1375"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>Import zakończony. Jaki był czas rozpoczęcia zapisu?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1445"/>
        <source>Oximeter Starting time</source>
        <translation>Czas uruchomienia pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1457"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>Chcę użyć czasu wewnętrznego zegara pulsoksymetru.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1534"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uwaga: zsynchronizowanie z z rozpoczęciem sesji CPAP zawsze będzie dokładniejsze.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1555"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>Wybierz sesję CPAP do synchronizacji:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1675"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>Tu możesz ręcznie dopasować czas, jeśli potrzebne:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1696"/>
        <source>HH:mm:ssap</source>
        <translation>HH:mm:ssap</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1793"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Skasuj</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1774"/>
        <source>&amp;Information Page</source>
        <translation>&amp;Strona informacyjna</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="688"/>
        <source>Set device date/time</source>
        <translation>Ustaw czas i datę urządzenia</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="695"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Zaznacz w celu włączenia uaktualnienia identyfikatora urządzenia przy następnym imporcie. Przydatne jak masz kilka pulsoksymetrów.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="698"/>
        <source>Set device identifier</source>
        <translation>Ustaw identyfikator urządzenia</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="761"/>
        <source>Erase session after successful upload</source>
        <translation>Skasuj sesję po udanym przesłaniu</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="793"/>
        <source>Import directly from a recording on a device</source>
        <translation>Importuj bezpośrednio z urządzenia</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="918"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Przypomnienie dla użyszkodników CPAP: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Czy pamiętałeś najpierw zaimportować sesję CPAP?&lt;br/&gt;&lt;/span&gt;Jeśli zapomnisz, nie będziesz miał właściwego czasu do synchronizacji tej sesji pulsoksymetru..&lt;br/&gt;Aby zapewnić dobrą synchronizację urządzeń, zawsze staraj się startować oba w jednym czasie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1325"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>Proszę wybierz, który chcesz importować do programu</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1399"/>
        <source>Day recording (normally would have) started</source>
        <translation>Dzienny zapis (normalnie powinien) zaczął się</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1473"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP device.</source>
        <translation>Wystartowałem pulsoksymetr o niemal tym samym czasie co aparat CPAP.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1502"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR potrzebuje czasu rozpoczęcia aby wiedzieć, gdzie zaimportować tę sesję pulsoksymetru.&lt;/p&gt;&lt;p&gt;Wybierz jedną z następujących opcji:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1812"/>
        <source>&amp;Retry</source>
        <translation>&amp;Spróbuj ponownie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1831"/>
        <source>&amp;Choose Session</source>
        <translation>&amp;Wybierz sesję</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1850"/>
        <source>&amp;End Recording</source>
        <translation>&amp;Koniec zapisu</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1869"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Synchronizuj i zapisz</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1888"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Zapisz i zamknij</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1907"/>
        <source>&amp;Start</source>
        <translation>&amp;Rozpocznij</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="190"/>
        <source>Scanning for compatible oximeters</source>
        <translation>Poszukiwanie kompatybilnego pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="222"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>Nie wykryto podłączonego pulsoksymetru.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="230"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>Łączenie z pulsoksymetrem %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="258"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Zmiana nazwy tego pulsoksymetru z %1&apos; na %2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="261"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>Inna nazwa pulsoksymetru. Jeżeli masz tylko jeden, i używasz go w obu profilach, ustaw jednakową nazwę w obu profilach.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="304"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot; sesja %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Nothing to import</source>
        <translation>Nic do zaimportowania</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="330"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>Pulsoksymetr nie zawiera ważnych sesji.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="331"/>
        <source>Close</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="334"/>
        <source>Waiting for %1 to start</source>
        <translation>Czekam na %1 z rozpoczęciem</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="335"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>Czekam aż urządzenie rozpocznie przesył danych...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Select upload option on %1</source>
        <translation>Wybierz opcję przesyłu danych na %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="338"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>Musisz powiedzieć pulsoksymetrowi żeby zaczął wysyłać dane do komputera.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="339"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>Proszę podłącz pulsoksymetr, wejdź do jego menu i wybierz przesyłanie danych...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="369"/>
        <source>%1 device is uploading data...</source>
        <translation>Urządzenie %1 wysyła dane...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="370"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Poczekaj aż pulsoksymetr zakończy przesyłanie danych. Nie odłączaj pulsoksymetru.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="389"/>
        <source>Oximeter import completed..</source>
        <translation>Import danych pulsoksymetru zakończony..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="414"/>
        <source>Select a valid oximetry data file</source>
        <translation>Wybierz ważny plik danych pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="414"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Pliki pulsoksymetru (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="436"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Żaden moduł pulsoksymetrii nie mógł przeanalizować danego pliku:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="484"/>
        <source>Live Oximetry Mode</source>
        <translation>Tryb live pulsoksymetrii</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="536"/>
        <source>Live Oximetry Stopped</source>
        <translation>Tryb live pulsoksymetrii zatrzymany</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="537"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>Import w trybie live pulsoksymetrii zatrzymany</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1097"/>
        <source>Oximeter Session %1</source>
        <translation>Sesja pulsoksymetru %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1142"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCAR daje możliwość śledzenia danych pulsoksymetru razem z danymi CPAP, co daje wgląd na efektywność leczenia. Można też przeglądać dane osobno.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1153"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>Jeżeli próbujesz zsynchronizować dane pulsoksymetru i CPAP, upewnij się, że zaimportowałeś sesje CPAP najpierw!!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1156"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>Aby OSCAR mógł prawidłowo znaleźć i zaimportować dane, potrzebujesz sterowników (np. USB to Serial UART). Szukaj ich %1 tutaj%2.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="466"/>
        <source>Oximeter not detected</source>
        <translation>Nie znaleziono pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="473"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>Nie ma dostępu do pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="487"/>
        <source>Starting up...</source>
        <translation>Startuję ...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="488"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Jeśli po paru sekundach nadal to widzisz, skasuj i spróbuj ponownie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="535"/>
        <source>Live Import Stopped</source>
        <translation>Import w trybie live pulsoksymetrii zatrzymany</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="588"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 sesje na %2, początek o %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="592"/>
        <source>No CPAP data available on %1</source>
        <translation>Brak danych CPAP na %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="727"/>
        <source>Recording...</source>
        <translation>Zapisuję...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="734"/>
        <source>Finger not detected</source>
        <translation>Nie wykryto palca</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="834"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Chcę użyć czasu który mój komputer użył do tej sesji live pulsoksymetru.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="837"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Potrzebuję ręcznie ustawić czas, bo mój pulsoksymetr nie ma zegara.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="849"/>
        <source>Something went wrong getting session data</source>
        <translation>Coś poszło nie tak z pozyskaniem danych sesji</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1138"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Witaj w Kreatorze importu pulsoksymetrii</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1140"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>Pulsoksymetry to urządzenia medyczne używane do pomiaru saturacji tlenem krwi. Podczas długotrwałego bezdechu oraz u pacjentów oddychających nieprawidłowo saturacja krwi tlenem obniża się znacznie, mogąc powodować problemy wymagające obserwacji medycznej.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1146"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>Możesz zauważyć, że niektóre firmy rebrandują pulsoksymetry,jak np. Contec CMS50xx pod nową nazwą jak np. Pulox PO-200, PO-300, PO-400. One też powinny współpracować.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>Może także odczytywać pliki .dat z pulsoksymetru ChoiceMMed MD300W1.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1151"/>
        <source>Please remember:</source>
        <translation>Proszę zapamiętaj:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1155"/>
        <source>Important Notes:</source>
        <translation>Ważne notatki:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1158"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Urządzenia Contec CMS50D+ nie mają wbudowanego zegara i nie rejestrują czasu rozpoczęcia sesji. Jeżeli nie masz sesji CPAP do podpięcia zapisu, musisz ręcznie wpisać czas rozpoczęcia po zakończeniu importu.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1160"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Nawet dla urządzeń z wewnętrznym zegarem warto wykształcić nawyk rozpoczynania sesji pulsoksymetru razem z CPAP, gdyż zegary CPAP potrafią być niedokładne i nie zawsze łatwo je przestawić.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1144"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR obecnie współpracuje z modelami Contec CMS50D+, CMS50E, CMS50I.( Uwaga - bezpośrednie importowanie przez bluetooth raczej niemożliwe.)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="717"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Tu możesz wpisać 7-literową nazwę dla tego pulsoksymetru.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="758"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ta opcja wykasuje importowane sesje z Twojego pulsoksymatru po ukończeniu importu &lt;/p&gt;&lt;p&gt;Używaj ostrożnie, ponieważ w wypadku utraty danych nie da się już ich przywrócić.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="787"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ta opcja pozwala na import danych zapisanych w pulsoksymetrze (przez kabel usb).&lt;/p&gt;&lt;p&gt;Po wybraniu tej opcji starsze pulsoksymetry Contex wymagają włączenia wysyłki menu urządzenia.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1000"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation>Porzę podłączyć pulsoksymetr, włączyć i wejść do menu</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>d/MM/yy h:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>R&amp;esetuj</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;Otwórz plik .spo/R</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>&amp;Import seryjny</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>&amp;Rozpocznij sesję live</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Port szeregowy</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;Ponownie skanuj porty</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="29"/>
        <source>Preferences</source>
        <translation>Preferencje</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="70"/>
        <source>&amp;Import</source>
        <translation>&amp;Importuj</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="160"/>
        <source>Combine Close Sessions </source>
        <translation>Połącz krótkie sesje </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="170"/>
        <location filename="../oscar/preferencesdialog.ui" line="255"/>
        <location filename="../oscar/preferencesdialog.ui" line="753"/>
        <source>Minutes</source>
        <translation>Minuty</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="190"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Wielokrotne sesje bliższe siebie niż dana wielkość będą zachowane w tym samym dniu.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="245"/>
        <source>Ignore Short Sessions</source>
        <translation>Ignoruj krótkie sesje</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="311"/>
        <source>Day Split Time</source>
        <translation>Czas podziału dnia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="321"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Sesje zaczynające się przed tym czasem będą zapisane z dniem poprzedzającym.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="394"/>
        <source>Session Storage Options</source>
        <translation>Opcje zapisu sesji</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="441"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Kompresuj kopie zapasowe kart SD (pierwszy import wolniej, ale kopie będą mniejsze)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="475"/>
        <source>This maintains a backup of SD-card data for ResMed devices, 

ResMed S9 series devices delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>To obsługuje kopię zapasową danych aparatów ResMed,

Aparaty ResMed S9 usuwają dokładniejsze dane starsze niż 7 dni,
oraz dane wykresów starsze niz 30 dni.

OSCAR może zachować te dane jeśli kiedyś będziesz reinstalował.
(Rekomendowane, o ile nie masz za mało miejsca albo nie dbasz o dane wykresów)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="635"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any device model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyświetl ostrzeżenie przy imporcie danych z aparatu nie testowanego przez deweloperów OSCARa.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="638"/>
        <source>Warn when importing data from an untested device</source>
        <translation>Ostrzegaj przed importem danych z nie testowanego aparatu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="659"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="803"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP device. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>Te obliczenia wymagają danych o wycieku całkowitym dostarczonych pzez aparat CPAP (np. PRS1, ale nie ResMed które już to mają)

Obliczenia niezamierzonych wycieków są liniowe, nie uwzględniają krzywej wentylacji maski.

Jeżeli używasz kilku różnych masek, wybierz wartosć średnią. Powinno wystarczyć.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="967"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the device missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>Włącz/wyłącz eksperymentalne wzmocnienie oznaczanie zdarzeń.
Pozwala to na wykrycie zdarzeń granicznychi takich, które przegapił aparat.
Ta opcja musi być włączona przed importem, w innym przypadku wymagane jest czyszczenie danych.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1019"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve device detected event positioning.</source>
        <translation>Ta eksperymentalna opcja próbuje użyć systemu flagowania OSCAR by poprawić pozycjonowanie zdarzeń wykrytych przez aparat.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1022"/>
        <source>Resync Device Detected Events (Experimental)</source>
        <translation>Resynchronizuj zdarzenia wykryte przez aparat (eksperymentalne)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1142"/>
        <source>Allow duplicates near device events.</source>
        <translation>Pozwalaj na duplikaty zdarzeń bliskich .</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1190"/>
        <source>Show flags for device detected events that haven&apos;t been identified yet.</source>
        <translation>Pokaż flagi zdarzeń wykrytych przez aparat które dotąd nie zostały zidentyfikowane.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1297"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>Uwzględniaj dni z użyciem mniejszym jako &quot;niewystarczające&quot;. Użycie 4-godzinne zwykle jest uważane za wystarczające.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1300"/>
        <source> hours</source>
        <translation> godziny</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1058"/>
        <source>Flow Restriction</source>
        <translation>Ograniczenia przepływu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1095"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>Procent ograniczenia przepływu z wartości mediany. 
Wartość 20% jest wystarczająca dla wykrycia bezdechu. </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1116"/>
        <source>Duration of airflow restriction</source>
        <translation>Czas ograniczenia przepływu powietrza</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="999"/>
        <location filename="../oscar/preferencesdialog.ui" line="1119"/>
        <location filename="../oscar/preferencesdialog.ui" line="1592"/>
        <location filename="../oscar/preferencesdialog.ui" line="1683"/>
        <location filename="../oscar/preferencesdialog.ui" line="1712"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1155"/>
        <source>Event Duration</source>
        <translation>Czas trwania zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1220"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Dopasuj wielkość danych uwzględnianych dla każdego punktu wykresu AHI/godz.
Domyślnie to 60 min. Zalecamy pozostawić tą wartość.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1224"/>
        <source> minutes</source>
        <translation> min</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1262"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>Zresetuj licznik do zera na początku każdego okienka (czasu).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1265"/>
        <source>Zero Reset</source>
        <translation>Reset zero</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="706"/>
        <source>CPAP Clock Drift</source>
        <translation>Niedokładność zegara CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="502"/>
        <source>Do not import sessions older than:</source>
        <translation>Nie importuj sesji starszych niż:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="509"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>Sesje starsze od tej daty nie będą importowane</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="535"/>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM yyyy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1278"/>
        <source>User definable threshold considered large leak</source>
        <translation>Definiowany przez użytkownika próg uważany za duży wyciek</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1245"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Czy i kiedy pokazać na wykresie wycieku czerwoną linię</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1832"/>
        <location filename="../oscar/preferencesdialog.ui" line="1911"/>
        <source>Search</source>
        <translation>Wyszukaj</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1518"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Pulsoksymetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1109"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Pokaż na wykresie kołowym zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1735"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>Procentowy spadek wysycenia tlenem</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1728"/>
        <source>Pulse</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1693"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>nagła zmiana pulsu co najmniej o wielkości</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1582"/>
        <location filename="../oscar/preferencesdialog.ui" line="1615"/>
        <location filename="../oscar/preferencesdialog.ui" line="1696"/>
        <source> bpm</source>
        <translation> uderzeń na minutę</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1680"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Minimalny czas trwania spadku wysycenia tlenem</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1709"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>Minimalny czas trwania zdarzenia zmiany pulsu.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1589"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Małe ilości danych pulsoksymetru (mniejsze niż ta wartość) będą odrzucane.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1967"/>
        <source>&amp;General</source>
        <translation>&amp;Ogólne</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1345"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Zmiana następujących ustawień wymaga restartu, ale nie przeliczania.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1348"/>
        <source>Preferred Calculation Methods</source>
        <translation>Preferowane metody obliczania</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1377"/>
        <source>Middle Calculations</source>
        <translation>Obliczenia pośrednie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1391"/>
        <source>Upper Percentile</source>
        <translation>Górny percentyl</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="97"/>
        <source>Session Splitting Settings</source>
        <translation>Ustawienia podziału sesji</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="357"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;To ustawienie powinno być używane ostrożnie...&lt;/span&gt;Wyłączenie skutkuje dokładnością dla dni tylko z podsumowaniem, ponieważ określone obliczenia będą poprawne gdy będą przechowywane w tym samym miejscu. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Użytkownicy ResMed:&lt;/span&gt; Tylko dlatego, że wydaje się naturalnym  że  12-godzinne wznowienie sesji powinno nastąpić poprzedniego dnia, to nie znaczy, ze dane ResMed się z tym zgadzają. Format pliku STF.edf wskaźników podsumowania ma poważne słabości, które czynią to kiepskim pomysłem.&lt;/p&gt;&lt;p&gt;Ta opcja jest dla uspokojenia tych co mimo wszystko chcą to widzieć &amp;quot;naprawione&amp;quot; bez względu na koszty, które wystąpią. Jeżeli będziesz trzymał swoją kartę SD w aparacie każdą noc, a importował dane co tydzień, nieczęsto będziesz widział podobne problemy.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="360"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>Nie dziel dni w podsumowaniu(Uwaga: czytaj wskazówki!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="569"/>
        <source>Memory and Startup Options</source>
        <translation>Opcje pamięci i rozruchu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="611"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>Ładuj dane podumowań na starcie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To ustawienie utrzymuje dane wykresów i zdarzeń w pamięci dla przyspieszenia przy ponownym wyświetlaniu.&lt;/p&gt;&lt;p&gt;To nie jest konieczne, gdyż system również przechowuje wcześniej używane pliki.&lt;/p&gt;&lt;p&gt;Rekomenduje się pozostawienie wyłączonym, chyba że komputer cierpi na nadmiar pamięci.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="601"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Pozostaw dane wykresów i zdarzeń w pamięci</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="625"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wycina jakiekolwiek niepotrzebne potwierdzenia podczas importu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="628"/>
        <source>Import without asking for confirmation</source>
        <translation>Importuj bez pytania o zgodę</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1184"/>
        <source>General CPAP and Related Settings</source>
        <translation>Ogólne ustawienia CPAP i pochodnych</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>Enable Unknown Events Channels</source>
        <translation>Uruchom kanał nieznanych zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1323"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1328"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation>RDI - indeks zaburzeń oddychania</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1200"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>Okno czasowe wykresu AHI/godzina</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1255"/>
        <source>Preferred major event index</source>
        <translation>Preferowany indeks głównych zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1207"/>
        <source>Compliance defined as</source>
        <translation>Zgodność definiowana jako</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1248"/>
        <source>Flag leaks over threshold</source>
        <translation>Zaznaczaj (flaguj) przecieki powyżej wątku</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="787"/>
        <source>Seconds</source>
        <translation>Sekundy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="733"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Uwaga: To nie jest przeznaczone do korekty strefy czasowej! Upewnij się, że zegar systemowy komputera i strefa czasowa są ustawione poprawnie.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="780"/>
        <source>Hours</source>
        <translation>Godziny</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1354"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>Dla spójności, użytkownicy ResMed powinni używać tu 95%,
ponieważ jest to jedyna wartość dostępna dla dni tylko z podsumowaniem.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1405"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>Mediana jest rekomendowana dla użytkowników ResMed.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <location filename="../oscar/preferencesdialog.ui" line="1472"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1414"/>
        <source>Weighted Average</source>
        <translation>Średnia ważona</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1419"/>
        <source>Normal Average</source>
        <translation>Średnia arytmetyczna</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>True Maximum</source>
        <translation>Prawdziwe maksimum</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1448"/>
        <source>99% Percentile</source>
        <translation>Percentyl 99%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1384"/>
        <source>Maximum Calcs</source>
        <translation>Obliczenia maksimum</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1981"/>
        <source>General Settings</source>
        <translation>Ogólne ustawienia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2722"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>Przyciski nawigacji w widoku dziennym będą przeskakiwały nad dniami bez danych</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2725"/>
        <source>Skip over Empty Days</source>
        <translation>Pomijaj dni bez danych</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2002"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Zezwalaj na użycie wielu rdzeni procesora gdy to możliwe, by poprawić wydajność.
Głównie wpływa na import.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2006"/>
        <source>Enable Multithreading</source>
        <translation>Włącz wielowątkowość</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="575"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Omiń ekran logowania i załaduj ostatni profil użytkownika</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="484"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Utwórz kopię zapasową danych karty SD podczas importu(wyłącz na własną zgubę!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Prawdziwym maksimum jest maksimum zestawu danych.&lt;/p&gt;&lt;p&gt;99% percentyl filtruje najrzadsze odchylenia.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1457"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>Połączona liczba podzielona przez całkowite godziny</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1462"/>
        <source>Time Weighted average of Indice</source>
        <translation>Średnia ważona czasu wskaźników</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1467"/>
        <source>Standard average of indice</source>
        <translation>Średnia arytmetyczna wskaźników</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="972"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Własne oznakowanie zdarzeń CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1809"/>
        <source>Events</source>
        <translation>Zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1773"/>
        <location filename="../oscar/preferencesdialog.ui" line="1862"/>
        <location filename="../oscar/preferencesdialog.ui" line="1941"/>
        <source>Reset &amp;Defaults</source>
        <translation>Przywróć &amp;Domyślne</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1875"/>
        <location filename="../oscar/preferencesdialog.ui" line="1954"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Uwaga: &lt;/span&gt;To, że można, nie znaczy, że to dobry pomysł.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1888"/>
        <source>Waveforms</source>
        <translation>Wykresy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1661"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Zaznacz (oflaguj) szybkie zmiany w pulsoksymetrii</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1569"/>
        <source>Other oximetry options</source>
        <translation>Inne opcje pulsoksymetrii</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1602"/>
        <source>Discard segments under</source>
        <translation>Odrzuć segmenty ponizej</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1642"/>
        <source>Flag Pulse Rate Above</source>
        <translation>Oflaguj częstość tętna powyżej</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1632"/>
        <source>Flag Pulse Rate Below</source>
        <translation>Oflaguj częstość tętna poniżej</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1622"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Flag SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt; Desaturations Below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Oznacz SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt; Desaturacje poniżej&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2121"/>
        <source>Check for new version every</source>
        <translation>Sprawdzaj w poszukiwaniu nowej wersji co</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2144"/>
        <source>days.</source>
        <translation>dni.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2174"/>
        <source>Last Checked For Updates: </source>
        <translation>Ostatnio sprawdzano uaktualnienia: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2187"/>
        <source>TextLabel</source>
        <translation>Etykieta tekstowa</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2255"/>
        <source>&amp;Appearance</source>
        <translation>&amp;Wygląd</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2284"/>
        <source>Graph Settings</source>
        <translation>Ustawienia wykresów</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2558"/>
        <source>Bar Tops</source>
        <translation>Nagłówki pasków</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2563"/>
        <source>Line Chart</source>
        <translation>Wykresy liniowe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2653"/>
        <source>Overview Linecharts</source>
        <translation>Przeglądowe wykresy liniowe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;To ułatwia przewijanie gdy powiększone na czułych dwukierunkowych touchpadach&lt;/p&gt;&lt;p&gt;50ms jest wartością rekomendowaną.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2492"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Jak długo mają być widoczne wskazówki.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2480"/>
        <source>Scroll Dampening</source>
        <translation>Tłumienie przewijania</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2470"/>
        <source>Tooltip Timeout</source>
        <translation>Limit czasu podpowiedzi</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2577"/>
        <source>Default display height of graphs in pixels</source>
        <translation>Domyślna wysokość wykresów w pikselach</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2550"/>
        <source>Graph Tooltips</source>
        <translation>Podpowiedzi dla wykresów</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2426"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>Metoda wizualna wyświetlania nakładających się flag wykresów.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2431"/>
        <source>Standard Bars</source>
        <translation>Paski standardowe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2436"/>
        <source>Top Markers</source>
        <translation>Najlepsze znaczniki</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2384"/>
        <source>Graph Height</source>
        <translation>Wysokość wykresów</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="433"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>Kompresuj kopie zapasowa ResMed (EDF) dla oszczędzenia miejsca na dysku. Pliki EDF są spakowane w formacie *.gz, jak to wiedzą użytkownicy makówek i linuksiarze.

OSCAR umie tego używać. ResScan potrzebuje rozpakowanych plików.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="453"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>Następujące opcje wpływają na zajęcie miejsca na dysku przez program, a także na szybkość importu.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="463"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>To powoduje, że OSCAR zabiera o połowę mniej miejsca na dysku.
Ale import i zmiana dnia zabiera więcej czasu..
Jeśli masz nowy komputer z małym dyskiem SSD to jest dobry pomysł.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="468"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>Kompresuj dane sesji (folder danych mniejszy, ale wolniejsza zmiana dni)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="578"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>Automatycznie otwieraj importowanie po otwarciu profilu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="608"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR startuje nieco wolniej, ładuje dane podsumowań z wyprzedzeniem, ale potem działa szybciej. &lt;/p&gt;&lt;p&gt;Jeśli masz dużo danych, to wyłącz, chyba, że lubisz widzieć wszystko. &lt;/p&gt;&lt;p&gt;Zauważ, że to ustawienie nie wpływa na dane wykresów i zdarzeń.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="618"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>Automatycznie otwieraj ostatnio używany profil po uruchomieniu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="810"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Wylicz niezamierzone wycieki jeśli nieobecne</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="906"/>
        <source>4 cmH2O</source>
        <translation>4 cmH20</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="916"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="948"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Uwaga: Użyto liniowej metody obliczania. Zmiana tych wartości wymaga przeliczenia.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1281"/>
        <source> l/min</source>
        <translation> l/min</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1398"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cumulative Indices&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wskaźniki skumulowane&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1539"/>
        <source>Oximetry Settings</source>
        <translation>Ustawienia pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2013"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>Pokazuj przypomnienie Usuń Kartę przy zamykaniu programu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2212"/>
        <source>I want to be notified of test versions. (Advanced users only please.)</source>
        <translation>Chcę otrzymywać powiadomienia o wersjach testowych. (Tylko dla zaawansowanych użytkowników.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2290"/>
        <source>On Opening</source>
        <translation>Przy otwarciu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2300"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Którą zakładkę otwierać przy ładowaniu. Uwaga - domyślnie otworzy Profile.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2303"/>
        <location filename="../oscar/preferencesdialog.ui" line="2307"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2312"/>
        <location filename="../oscar/preferencesdialog.ui" line="2351"/>
        <source>Welcome</source>
        <translation>Witaj</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2317"/>
        <location filename="../oscar/preferencesdialog.ui" line="2356"/>
        <source>Daily</source>
        <translation>Dziennie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2327"/>
        <location filename="../oscar/preferencesdialog.ui" line="2366"/>
        <source>Statistics</source>
        <translation>Statystyki</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2335"/>
        <source>Switch Tabs</source>
        <translation>Przełącz zakładki</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2346"/>
        <source>No change</source>
        <translation>Bez zmian</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2374"/>
        <source>After Import</source>
        <translation>Po imporcie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2400"/>
        <source>Overlay Flags</source>
        <translation>Nakładające się flagi</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2410"/>
        <source>Line Thickness</source>
        <translation>Grubość linii</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2446"/>
        <source>The pixel thickness of line plots</source>
        <translation>Wielkość piksela wykresów liniowych</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2672"/>
        <source>Other Visual Settings</source>
        <translation>Inne ustawienia wizualne</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2678"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>Anti-Aliasing daje wygładzenie wykresów,,.
Niektóre wykresy wyglądają lepiej. 
Również wydruki raportów

Spróbuj i zdecyduj.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2685"/>
        <source>Use Anti-Aliasing</source>
        <translation>Użyj Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2692"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Powoduje, że wyglad niektórych wykresów jest bardziej prostokątny.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2695"/>
        <source>Square Wave Plots</source>
        <translation>Wykresy prostokątne</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2702"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Buforowanie mapy pikseli jest techniką przyspieszania grafiki. Może spowodować problemy z wyświetlaniem czcionek na twoim systemie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2705"/>
        <source>Use Pixmap Caching</source>
        <translation>Użyj buforowania mapy pikseli</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2712"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;head/&gt;&lt;body&gt;&lt;p&gt;Te funkcje ostatnio usunięto. Wrócą później. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2715"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>Animacje &amp;&amp; Duperelki</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2732"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Czy pozwolić na zmianę skali osi Y przez dwuklik na etykietach osi Y</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2735"/>
        <source>Allow YAxis Scaling</source>
        <translation>Pozwól na skalowanie osi Y</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2046"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Silnik graficzny (wymaga restartu)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="272"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;śesje trwające krócej nie będą wyświetlane&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1076"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the device. They are &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; included in AHI.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Flagowanie przez użytkownika jest eksperymentalną metodą wykrywania zdarzeń pominiętych przez urządzenie.Są one &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; włączone do AHI.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1486"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed devices do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Z uwagi na ograniczenia, aparaty ResMed nie obsługują zmiany tych ustawień.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1795"/>
        <source> &lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt; p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method do &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP device, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation> &lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt; p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Synchronizowanie danych oksymetrii i CPAP&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Dane CMS50 importowane z SpO2Review (from .spoR files) lub serial import  &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;nie&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; mają właściwego znacznika czasowego potrzebnego do synchronizacji.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Tryb Live view (z użyciem kabla serial) jest jedynym sposobem do uzyskania właściwej synchronizacji pulsoksymetrów CMS50 oximeters, ale nie uwzględnia niedokładności zegara urządzenia CPAP.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Jeżeli właczysz pulsoksymetr &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;równocześnie &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;z aparatem CPAP, to możesz uzyskać synchronizację. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Import przez usb pobiera czas startu z ostatniej sesji - pamiętaj, najpierw wgraj dane z karty SD!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2052"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>Spróbuj to zmienić z ustawień domyślnych (Desktop OpenGL) jeśli doświadczasz problemów renderowania z wykresami OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2742"/>
        <source>Whether to include device serial number on device settings changes report</source>
        <translation>Czy zaimportować nr seryjny urządzenia w raporcie zmian ustawień urządzenia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2787"/>
        <source>Fonts (Application wide settings)</source>
        <translation>Czcionki (Ustawienia dla całej aplikacji)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2821"/>
        <source>Font</source>
        <translation>Czcionka</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2839"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2857"/>
        <source>Bold  </source>
        <translation>Wytłuszczenie  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2878"/>
        <source>Italic</source>
        <translation>Pochylenie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2891"/>
        <source>Application</source>
        <translation>Aplikacja</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2955"/>
        <source>Graph Text</source>
        <translation>Tekst wykresu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3016"/>
        <source>Graph Titles</source>
        <translation>Tytuły wykresu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3077"/>
        <source>Big  Text</source>
        <translation>Duży  tekst</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3143"/>
        <location filename="../oscar/preferencesdialog.cpp" line="472"/>
        <location filename="../oscar/preferencesdialog.cpp" line="604"/>
        <source>Details</source>
        <translation>Szczegóły</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3175"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Skasuj</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3182"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="467"/>
        <location filename="../oscar/preferencesdialog.cpp" line="598"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="468"/>
        <location filename="../oscar/preferencesdialog.cpp" line="599"/>
        <source>Color</source>
        <translation>Kolor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="470"/>
        <source>Flag Type</source>
        <translation>Typ flagi</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="471"/>
        <location filename="../oscar/preferencesdialog.cpp" line="603"/>
        <source>Label</source>
        <translation>Etykieta</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="488"/>
        <source>CPAP Events</source>
        <translation>Zdarzenia CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="489"/>
        <source>Oximeter Events</source>
        <translation>Zdarzenia pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="490"/>
        <source>Positional Events</source>
        <translation>Zdarzenia ułożenia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="491"/>
        <source>Sleep Stage Events</source>
        <translation>Zdarzenia fazy snu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="492"/>
        <source>Unknown Events</source>
        <translation>Nieznane zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="664"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>Dwuklik dla zmiany nazwy opisowej tego kanału.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="542"/>
        <location filename="../oscar/preferencesdialog.cpp" line="671"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>Dwuklik dla zmiany domyślnego koloru wykresu/flagi/danych tego kanału.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2322"/>
        <location filename="../oscar/preferencesdialog.ui" line="2361"/>
        <location filename="../oscar/preferencesdialog.cpp" line="469"/>
        <location filename="../oscar/preferencesdialog.cpp" line="600"/>
        <source>Overview</source>
        <translation>Przegląd</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="534"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>Dwuklik dla zmiany nazwy opisowej kanału &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="547"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Czy ta flaga ma dedykowaną tabelę przeglądową.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="557"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Tu możesz zmienić typ flagi pokazywanej dla tego zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="562"/>
        <location filename="../oscar/preferencesdialog.cpp" line="695"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>To jest skrócona etykieta dla wskazania tego kanału na ekranie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="568"/>
        <location filename="../oscar/preferencesdialog.cpp" line="701"/>
        <source>This is a description of what this channel does.</source>
        <translation>To jest opis działania tego kanału.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="601"/>
        <source>Lower</source>
        <translation>Niższy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="602"/>
        <source>Upper</source>
        <translation>Wyższy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="621"/>
        <source>CPAP Waveforms</source>
        <translation>Wykresy CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="622"/>
        <source>Oximeter Waveforms</source>
        <translation>Wykresy pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="623"/>
        <source>Positional Waveforms</source>
        <translation>Wykresy ułożenia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="624"/>
        <source>Sleep Stage Waveforms</source>
        <translation>Wykresy fazy snu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="680"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Czy ten wykres wyświetla się w przeglądzie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="685"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Tu możesz ustawić &lt;b&gt;niższy &lt;/b&gt; próg używany do określonych obliczeń dla wykresu %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="690"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Tu możesz ustawić &lt;b&gt;wyzszy &lt;/b&gt; próg używany do określonych obliczeń dla wykresu %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="800"/>
        <source>Data Processing Required</source>
        <translation>Wymagane przetworzenie danych</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="801"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Kompresja/dekompresja danych jest wymagana do zastosowania tych zmian. Ta operacja może zpotrwać kilka minut.

Na pewno chcesz dokonać tych zmian?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="809"/>
        <source>Data Reindex Required</source>
        <translation>Wymagana reindeksacja danych</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="810"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Reindeksacja danych jest wymagana do zastosowania tych zmian. Ta operacja może zpotrwać kilka minut.

Na pewno chcesz dokonać tych zmian?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="816"/>
        <source>Restart Required</source>
        <translation>Wymagany restart</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1173"/>
        <source>ResMed S9 devices routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>Aparaty ResMed S9 rutynowo usuwają określone dane z karty SD starsze niż 7 i 30 dni (zależnie od rozdzielczości).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1174"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> Jeśli kiedykolwiek potzrbowałbyś tych danych - nie wrócą.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1175"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> Jeśli potrzebujesz oszczędzać miejsce na dysku, pamiętaj zrobić kopie zapasowe ręcznie.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1176"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> Jesteś pewny, że chcesz wyączyć kopie zapasowe?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1256"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>Wyłączenie kopii zapasowych to słaby pomysł, OSCAR moze ich potrzebować do przebudowania bazy danych w razie błędów.

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1257"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Na pewno chcesz to zrobić?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Flag</source>
        <translation>Flaga</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Minor Flag</source>
        <translation>Mała flaga</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Span</source>
        <translation>Odstęp</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="53"/>
        <source>Always Minor</source>
        <translation>Zawsze mniejsze</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="295"/>
        <source>Never</source>
        <translation>Nigdy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="817"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>Jedna lub więcej zmian wymaga restartu, o ile mają być widoczne.

Restartować teraz?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="66"/>
        <source>No CPAP devices detected</source>
        <translation>Nie wykryto aparatu CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="67"/>
        <source>Will you be using a ResMed brand device?</source>
        <translation>Czy będziesz używał aparatu ResMed?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="74"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; devices due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed devices, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Uwaga:&lt;/b&gt; zaawansowane dzielenie sesji nie jest możliwe dla aparatów ResMed z uwagi na ograniczenia w przechowywaniu danych i ustawień, i dlatego w tym profilu są wyłączone.&lt;/p&gt;&lt;p&gt;Dni będą dzielone w południe, jak w komercyjnym oprogramowaniu.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1172"/>
        <source>This may not be a good idea</source>
        <translation>To słaby pomysł</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="423"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation>Zmiana opcji kompresji danych na karcie SD nie powoduje automatycznej rekompresji danych zapasowych.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="819"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation>Wskaźnik wentylacji maski przy ciśnieniu 20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="875"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation>Wskaźnik wentylacji maski przy ciśnieniu 4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2745"/>
        <source>Include Serial Number</source>
        <translation>Dołącz numer seryjny</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Wyświetl ostrzeżenie, gdy importowane dane są inne niż wcześniej widziane przez deweloperów OSCARa&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="648"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation>Ostrzegaj, gdy wcześniej nieznane dane są zaliczane</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2020"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation>Zawsze zapisuj zrzuty ekranu w folderze danych OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2074"/>
        <source>Check For Updates</source>
        <translation>Sprawdź uaktualnienia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2089"/>
        <source>You are using a test version of OSCAR. Test versions check for updates automatically at least once every seven days.  You may set the interval to less than seven days.</source>
        <translation>Używasz testowej wersji OSCARa. Wersje testowe sprawdzają aktualizacje co najmniej co siedem dni. Możesz ustawić częściej.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2106"/>
        <source>Automatically check for updates</source>
        <translation>Automatycznie sprawdzaj aktualizacje</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2128"/>
        <source>How often OSCAR should check for updates.</source>
        <translation>Jak często OSCAR ma sprawdzać aktuallizacje.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>If you are interested in helping test new features and bugfixes early, click here.</source>
        <translation>Jeśli chcesz pomóc w testowaniu nowych funkcji i poprawek, kliknij tu.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2225"/>
        <source>If you would like to help test early versions of OSCAR, please see the Wiki page about testing OSCAR.  We welcome everyone who would like to test OSCAR, help develop OSCAR, and help with translations to existing or new languages. https://www.sleepfiles.com/OSCAR</source>
        <translation>Jeśli chcesz pomóc w testowaniu wczesnych wersji OSCARa, odwiedź stronę Wiki o testowaniu OSCAR. Zapraszamy wszystkich, którzy chcieliby przetestować OSCARa, pomóc w rozwoju OSCARa i pomóc w tłumaczeniach na istniejące lub nowe języki. https://www.sleepfiles.com/OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2752"/>
        <source>Print reports in black and white, which can be more legible on non-color printers</source>
        <translation>Drukuj raporty w czerni i bieli, co może być bardziej czytelne na drukarkach innych niż kolorowe</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2755"/>
        <source>Print reports in black and white (monochrome)</source>
        <translation>Drukuj raporty w czerni i bieli (monochromatycznie)</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filtr:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="198"/>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="215"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;Otwórz profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="226"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Edytuj profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="240"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Nowy profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="258"/>
        <source>Profile: None</source>
        <translation>Profil:Brak</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="278"/>
        <source>Please select or create a profile...</source>
        <translation>Proszę wybierz lub utwórz profil...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="328"/>
        <source>Destroy Profile</source>
        <translation>Zniszcz profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Brand</source>
        <translation>Marka aparatu</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Ventilator Model</source>
        <translation>Model aparatu</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Other Data</source>
        <translation>Inne dane</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Last Imported</source>
        <translation>Ostatnio importowane</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="233"/>
        <location filename="../oscar/profileselector.cpp" line="367"/>
        <source>Enter Password for %1</source>
        <translation>Wprowadź hasło dla %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="249"/>
        <location filename="../oscar/profileselector.cpp" line="386"/>
        <source>You entered an incorrect password</source>
        <translation>Wprowadziłeś nieprawidłowe hasło</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Forgot your password?</source>
        <translation>Zapomniałeś hasła?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Zapytaj na forum jak to zresetować, to łatwe.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="322"/>
        <source>Select a profile first</source>
        <translation>Najpierw wybierz profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="389"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Jeżeli próbujesz usunąć bo zapomniałeś hasła, musisz zresetować albo usunąć folder profilu ręcznie.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>Masz zamiar usunąć profil &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Pomyśl chwilę, to nieodwracalnie usunie profil i dane &lt;b&gt;kopii zapasowej&lt;/b&gt;, przechowywanych w &lt;/br&gt;%2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Wprowadź słowo &lt;b&gt;DELETE&lt;/b&gt; poniżej, dokładnie jak tu napisane by potwierdzić.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="417"/>
        <source>DELETE</source>
        <translation>DELETE</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>Sorry</source>
        <translation>Przepraszam</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Musisz wpisać DELETE uzywając WIELKICH liter.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="431"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Wystąpił błąd w usuwaniu katalogu profilu, musisz usunąć go ręcznie.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="435"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profil &apos;%1&apos; pomyślnie usunięto</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>Bytes</source>
        <translation>Bajtów</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="465"/>
        <source>Summaries:</source>
        <translation>Podsumowania:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="466"/>
        <source>Events:</source>
        <translation>Zdarzenia:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="467"/>
        <source>Backups:</source>
        <translation>Kopie zapasowe:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="479"/>
        <location filename="../oscar/profileselector.cpp" line="519"/>
        <source>Hide disk usage information</source>
        <translation>Ukryj informację o użyciu dysku</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="482"/>
        <source>Show disk usage information</source>
        <translation>Pokaż informację o użyciu dysku</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="500"/>
        <source>Name: %1, %2</source>
        <translation>Nazwisko: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Phone: %1</source>
        <translation>Telefon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="506"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="509"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="512"/>
        <source>No profile information given</source>
        <translation>Nie podano danych profilu</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>Profile: %1</source>
        <translation>Profil: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="170"/>
        <source>You must create a profile</source>
        <translation>Musisz utworzyć profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation>Wyczyść filtry aby ujrzeć wszystkie profile</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="357"/>
        <source>The selected profile does not appear to contain any data and cannot be removed by OSCAR</source>
        <translation>Wydaje się, że wybrany profil nie zawiera żadnych danych i nie może zostać usunięty przez OSCAR</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="57"/>
        <source>Abort</source>
        <translation>Przerwij</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="820"/>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1260"/>
        <source>No Data</source>
        <translation>Brak danych</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="379"/>
        <source>Events</source>
        <translation>Zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="377"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="380"/>
        <source>Duration</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="394"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 w zdarzeniach)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jan</source>
        <translation>Sty</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Feb</source>
        <translation>Lut</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Mar</source>
        <translation>Mar</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Apr</source>
        <translation>Kwi</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>May</source>
        <translation>Maj</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jun</source>
        <translation>Cze</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Jul</source>
        <translation>Lip</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Aug</source>
        <translation>Sie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Sep</source>
        <translation>Wrz</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Oct</source>
        <translation>Paź</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Nov</source>
        <translation>Lis</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Dec</source>
        <translation>Gru</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="220"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="238"/>
        <source>Min: %1</source>
        <translation>Min: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="269"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="279"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="274"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="284"/>
        <source>Max: </source>
        <translation>Max: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="303"/>
        <source>Max: %1</source>
        <translation>Max: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="309"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 dni): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="311"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 dzień): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="371"/>
        <source>% in %1</source>
        <translation>% w %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="377"/>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="45"/>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <source>Hours</source>
        <translation>Godziny</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="383"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="30"/>
        <source>
Length: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gUsageChart.cpp" line="96"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 niskie użycie, %2 bez użycia, z %3 dni (%4 zgodnych). Długość : %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="102"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sesje: %1 / %2 / %3 Długość: %4 / %5 / %6 Najdłuższa: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="222"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Długość: %3
Start: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="224"/>
        <source>Mask On</source>
        <translation>Maska założona</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="224"/>
        <source>Mask Off</source>
        <translation>Maska zdjęta</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="235"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Długość: %3
Start: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gTTIAChart.cpp" line="71"/>
        <source>TTIA:</source>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gTTIAChart.cpp" line="83"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <source>Minutes</source>
        <translation>minuty</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <source>Seconds</source>
        <translation>sekundy</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="697"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>Events/hr</source>
        <translation>zdarzeń/godz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="702"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>bpm</source>
        <translation>uderzeń/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="705"/>
        <source>Litres</source>
        <translation>Litry</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="706"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>Breaths/min</source>
        <translation>Oddechów/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <source>Severity (0-1)</source>
        <translation>Ciężkość (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>Degrees</source>
        <translation>Stopni</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2920"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="865"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="866"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="867"/>
        <source>Warning</source>
        <translation>Ostrzeżenie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="716"/>
        <source>Information</source>
        <translation>Informacja</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Busy</source>
        <translation>Zajęty</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <source>Please Note</source>
        <translation>Proszę zauważ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>Graphs Switched Off</source>
        <translation>Wykresy wyłączone</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <source>Sessions Switched Off</source>
        <translation>Sesje wyłączone</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="729"/>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="730"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Skasuj</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <source>&amp;Destroy</source>
        <translation>&amp;Zniszcz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>&amp;Save</source>
        <translation>&amp;Zachowaj</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>BMI</source>
        <translation>BMI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="735"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="317"/>
        <source>Weight</source>
        <translation>Waga</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Pulse Rate</source>
        <translation>Częstość pulsu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>Plethy</source>
        <translation>Pletyzmografia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <source>Pressure</source>
        <translation>Ciśnienie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="742"/>
        <source>Daily</source>
        <translation>Dziennie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <source>Overview</source>
        <translation>Przegląd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <source>Oximetry</source>
        <translation>Pulsoksymetria</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <source>Oximeter</source>
        <translation>Pulsoksymetr</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="748"/>
        <source>Event Flags</source>
        <translation>Flagi zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <source>Default</source>
        <translation>Domyślnie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="754"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="779"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2841"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="117"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2844"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="757"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <source>EEPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <source>Min EPAP</source>
        <translation>Min EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <source>Max EPAP</source>
        <translation>Max EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="762"/>
        <source>Min IPAP</source>
        <translation>Min IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="763"/>
        <source>Max IPAP</source>
        <translation>Max IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="764"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="118"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2846"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2905"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2932"/>
        <source>Humidifier</source>
        <translation>Nawilżacz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="776"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="782"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2832"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2849"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="791"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="795"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>RDI</source>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="804"/>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Insp. Time</source>
        <translation>Czas wdechu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Exp. Time</source>
        <translation>Czas wydechu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="814"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Resp. Event</source>
        <translation>Zdarzenie oddechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Flow Limitation</source>
        <translation>Ograniczenie przepływu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="816"/>
        <source>Flow Limit</source>
        <translation>Limit przepływu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1024"/>
        <source>SensAwake</source>
        <translation>Przebudzenie sensoryczne</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <source>Pat. Trig. Breath</source>
        <translation>Oddech wyw. przez pacjenta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <source>Tgt. Min. Vent</source>
        <translation>Docel.went.min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Target Vent.</source>
        <translation>Docelowa wentylacja.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Minute Vent.</source>
        <translation>Wentylacja minutowa.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Tidal Volume</source>
        <translation>Objętość oddechowa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="823"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Resp. Rate</source>
        <translation>Częstość oddechów</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2804"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Snore</source>
        <translation>Chrapanie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Leak</source>
        <translation>Nieszczelność</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <source>Leaks</source>
        <translation>Nieszczelności</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <source>Large Leak</source>
        <translation>Duży wyciek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>LL</source>
        <translation>LL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Total Leaks</source>
        <translation>Całkowite nieszczelności</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <source>Unintentional Leaks</source>
        <translation>Nieszczelności niezamierzone</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>MaskPressure</source>
        <translation>Ciśnienie maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="832"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Flow Rate</source>
        <translation>Przepływ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="333"/>
        <source>Sleep Stage</source>
        <translation>Faza snu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <source>Usage</source>
        <translation>Użycie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>Sessions</source>
        <translation>Sesje</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="836"/>
        <source>Pr. Relief</source>
        <translation>Ulga ciśnienia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <source>No Data Available</source>
        <translation>Brak dostępnych danych</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="214"/>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>Software Engine</source>
        <translation>Silnik oprogramowania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <source>Desktop OpenGL</source>
        <translation>Desktop OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>in</source>
        <translation>cal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>l/min</source>
        <translation>l/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="721"/>
        <source>Only Settings and Compliance Data Available</source>
        <translation>Dostępne są tylko ustawienia i zgodne dane</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="723"/>
        <source>Summary Data Only</source>
        <translation>Tylko dane podsumowujące</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <source>Bookmarks</source>
        <translation>Zakładki</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="842"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="774"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2836"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2838"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="113"/>
        <source>Mode</source>
        <translation>Tryb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="843"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="844"/>
        <source>Brand</source>
        <translation>Marka</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="845"/>
        <source>Serial</source>
        <translation>nr seryjny</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="846"/>
        <source>Series</source>
        <translation>seria</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="848"/>
        <source>Channel</source>
        <translation>Kanał</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="849"/>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="851"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Inclination</source>
        <translation>Nachylenie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="852"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Orientation</source>
        <translation>Kierunek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="855"/>
        <source>Name</source>
        <translation>Nazwisko</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="856"/>
        <source>DOB</source>
        <translation>Data urodzenia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="857"/>
        <source>Phone</source>
        <translation>telefon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="858"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="859"/>
        <source>Email</source>
        <translation>email</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="860"/>
        <source>Patient ID</source>
        <translation>ID pacjenta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="861"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="863"/>
        <source>Bedtime</source>
        <translation>do łóżka</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="864"/>
        <source>Wake-up</source>
        <translation>wstał/a</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="865"/>
        <source>Mask Time</source>
        <translation>czas z maską</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="866"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="129"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="206"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="126"/>
        <source>Unknown</source>
        <translation>nieznany</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="867"/>
        <source>None</source>
        <translation>Żaden</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="868"/>
        <source>Ready</source>
        <translation>Gotowy</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="870"/>
        <source>First</source>
        <translation>pierwszy</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="871"/>
        <source>Last</source>
        <translation>ostatni</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="872"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Start</source>
        <translation>Początek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="873"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>End</source>
        <translation>Koniec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="874"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="192"/>
        <source>On</source>
        <translation>Włącz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="875"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="792"/>
        <source>Off</source>
        <translation>Wyłącz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="877"/>
        <source>Yes</source>
        <translation>Tak</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="878"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="880"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="881"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="882"/>
        <source>Med</source>
        <translation>Med</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="884"/>
        <source>Average</source>
        <translation>Średnio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="885"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="224"/>
        <location filename="../oscar/SleepLib/common.cpp" line="886"/>
        <source>Avg</source>
        <translation>Śrd</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="222"/>
        <location filename="../oscar/SleepLib/common.cpp" line="887"/>
        <source>W-Avg</source>
        <translation>ŚrdWaż</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="154"/>
        <source>Your %1 %2 (%3) generated data that OSCAR has never seen before.</source>
        <translation>Twój %1 %2 (%3) wygenerował dane, których OSCAR nigdy wcześniej nie widział.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="155"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation>Dane zaimportowanie mogą nie być dokładne dlatego deweloperzy chcieliby kopię .zip Twoich plików danych z karty SD, dla pewności, ze OSCAR prawidłowo obsługuje dane.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="164"/>
        <source>Non Data Capable Device</source>
        <translation>Aparat nie zbierający danych</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="165"/>
        <source>Your %1 CPAP Device (Model %2) is unfortunately not a data capable model.</source>
        <translation>Twoje urządzenier CPAP %1 (model %2) nie udostępnia zgodnych danych.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="166"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this device.</source>
        <translation>Niestety OSCAR może śledzić tylko czas działania i podstawowe ustawienia tego aparatu.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="178"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="454"/>
        <source>Device Untested</source>
        <translation>Aparat nie testowany</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="179"/>
        <source>Your %1 CPAP Device (Model %2) has not been tested yet.</source>
        <translation>Twoje urządzenie CPAP %1 (model %2) nie zostało jeszcze przetestowane.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="180"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure it works with OSCAR.</source>
        <translation>Wydaje się na tyle podobny do innych urządzeń, że może działać, ale programiści chcieliby mieć kopię .zip karty SD tego urządzenia i pasujące raporty .pdf lekarza, aby upewnić się, że działa z OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="188"/>
        <source>Device Unsupported</source>
        <translation>Aparat nie wspierany</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="189"/>
        <source>Sorry, your %1 CPAP Device (%2) is not supported yet.</source>
        <translation>Przepraszamy, Twój aparat %1 (%2) nie jest jeszcze wspierany.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="190"/>
        <source>The developers need a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make it work with OSCAR.</source>
        <translation>Deweloperzy potrzebują kopii .zip karty SD oraz odpowiadającej kopii .pdf badań lekarskich, aby mogły one współpracować z OSCARem.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2962"/>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2961"/>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2854"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2856"/>
        <source>Flex Mode</source>
        <translation>Tryb Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2855"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>PRS1 tryb ulgi ciśnienia.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2859"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2860"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2861"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2863"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2887"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2889"/>
        <source>Rise Time</source>
        <translation>Czas wzrostu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2864"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2870"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2872"/>
        <source>Flex Level</source>
        <translation>Flex Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2871"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>Ustawienia ulgi ciśnienia PRS1.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2903"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humidifier Status</source>
        <translation>Status nawilżacza</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2904"/>
        <source>PRS1 humidifier connected?</source>
        <translation>Czy jest podłączony nawilżacz PRS1?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2907"/>
        <source>Disconnected</source>
        <translation>Odłączony</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2908"/>
        <source>Connected</source>
        <translation>Podłączony</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2694"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="523"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="872"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="225"/>
        <source>Getting Ready...</source>
        <translation>Przygotowuję...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="535"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="897"/>
        <source>Scanning Files...</source>
        <translation>Skanuję pliki...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="628"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="905"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="743"/>
        <location filename="../oscar/mainwindow.cpp" line="2338"/>
        <source>Importing Sessions...</source>
        <translation>Importuję sesje...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="778"/>
        <source>UNKNOWN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="780"/>
        <source>APAP (std)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="781"/>
        <source>APAP (dyn)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="782"/>
        <source>Auto S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="783"/>
        <source>Auto S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="784"/>
        <source>AcSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="788"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="789"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="790"/>
        <source>SoftPAP Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="793"/>
        <source>Slight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="797"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="798"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="799"/>
        <source>PSoft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="803"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="804"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="805"/>
        <source>PSoftMin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="809"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="810"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="811"/>
        <source>AutoStart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="817"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="818"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="819"/>
        <source>Softstart_Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="823"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="824"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="825"/>
        <source>Softstart_TimeMax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="829"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="830"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="831"/>
        <source>Softstart_Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="835"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="836"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="837"/>
        <source>PMaxOA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="841"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="842"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="843"/>
        <source>EEPAPMin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="847"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="848"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="849"/>
        <source>EEPAPMax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="853"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="854"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="855"/>
        <source>HumidifierLevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="859"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="860"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="861"/>
        <source>TubeType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="875"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="878"/>
        <source>ObstructLevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="877"/>
        <source>Obstruction Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="885"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="887"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="888"/>
        <source>rMVFluctuation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="895"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="897"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="898"/>
        <source>rRMV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="903"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="905"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="906"/>
        <source>PressureMeasured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="911"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="913"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="914"/>
        <source>FlowFull</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="919"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="921"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="922"/>
        <source>SPRStatus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="928"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="930"/>
        <source>Artifact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="931"/>
        <source>ART</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="936"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="938"/>
        <source>CriticalLeak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="939"/>
        <source>CL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="944"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="946"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="947"/>
        <source>eMO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="953"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="955"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="956"/>
        <source>eSO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="962"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="964"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="965"/>
        <source>eS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="971"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="973"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="974"/>
        <source>eFL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="980"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="982"/>
        <source>DeepSleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="983"/>
        <source>DS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="990"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="992"/>
        <source>TimedBreath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2748"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="368"/>
        <location filename="../oscar/mainwindow.cpp" line="754"/>
        <location filename="../oscar/mainwindow.cpp" line="2360"/>
        <source>Finishing up...</source>
        <translation>Kończę...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2957"/>
        <source>Hose Diameter</source>
        <translation>Średnica węża</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2958"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>Średnica węża podstawowego CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2985"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2987"/>
        <source>Auto On</source>
        <translation>Auto włączanie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2994"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2996"/>
        <source>Auto Off</source>
        <translation>Auto wyłączanie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3003"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3005"/>
        <source>Mask Alert</source>
        <translation>Alarm maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3012"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3014"/>
        <source>Show AHI</source>
        <translation>Pokaż AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3080"/>
        <source>Breathing Not Detected</source>
        <translation>Nie wykryto oddechu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3082"/>
        <source>BND</source>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3087"/>
        <source>Timed Breath</source>
        <translation>Czasowy oddech</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3088"/>
        <source>Machine Initiated Breath</source>
        <translation>Oddech inicjowany przez aparat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="993"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3089"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="37"/>
        <source>Windows User</source>
        <translation>Użytkownik Windows</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="485"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Uruchomienie Eksploratora Windows nie powiodło się</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="486"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Nie znaleziono explorer.exe na ścieżce uruchomienia Eksploratora Windows.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="551"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR prowadzi kopię zapasową danych z Twoich aparatów których używa w tym celu.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="552"/>
        <source>&lt;i&gt;Your old device data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Twoje stare dane z aparatu powinny być zregenerowane ponieważ ta cecha kopii zapasowej nie została wyłączona w preferencjach podczas poprzedniego importu.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="555"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>Jak dotąd OSCAR nie ma żadnych automatycznych kopii zapasowych dla tego aparatu.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="556"/>
        <source>This means you will need to import this device data again afterwards from your own backups or data card.</source>
        <translation>To oznacza, że potrzebujesz ponownie zaimportować dane urządzenia z zapasu lub karty danych.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="559"/>
        <source>Important:</source>
        <translation>Ważne:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="560"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>W razie wątpliwości kliknij przycisk Nie, aby wyjść i ręcznie wykonać kopię zapasową profilu, zanim ponownie uruchomisz program OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>Czy jesteś gotowy na aktualizację, aby uruchomić nową wersję OSCAR?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="564"/>
        <source>Device Database Changes</source>
        <translation>Zmiany bazy danych aparatów</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="575"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>Niestety, operacja czyszczenia nie powiodła się, co oznacza, że ta wersja OSCAR nie może się uruchomić.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="576"/>
        <source>The device data folder needs to be removed manually.</source>
        <translation>Folder danych urządzenia musi być usunięty ręcznie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="594"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>Czy chcesz włączyć automatyczne tworzenie kopii zapasowych, aby następnym razem, gdy nowa wersja OSCAR będzie musiała to zrobić, mogła je ponownie utworzyć?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="601"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR uruchomi teraz kreator importu, aby móc ponownie zainstalować dane%1.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="611"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR zakończy teraz, a następnie (spróbuje) uruchomić menedżera plików komputera, aby można ręcznie przywrócić swój profil:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="613"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>Użyj swojego menedżera plików, aby utworzyć kopię swojego katalogu profilu, a następnie zrestartuj OSCAR i zakończ proces aktualizacji.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="577"/>
        <source>This folder currently resides at the following location:</source>
        <translation>Ten folder aktualnie jest w położeniu:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="585"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>Odbudowa z kopii zapasowej %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Therapy Pressure</source>
        <translation>Ciśnienie lecznicze</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Inspiratory Pressure</source>
        <translation>Ciśnienie wdechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>Niskie ciśnienie wdechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>Wysokie ciśnienie wdechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Expiratory Pressure</source>
        <translation>Ciśnienie wydechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>Lower Expiratory Pressure</source>
        <translation>Niskie ciśnienie wydechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Higher Expiratory Pressure</source>
        <translation>Wysokie ciśnienie wydechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="141"/>
        <source>End Expiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Pressure Support</source>
        <translation>Wsparcie ciśnieniem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>PS Min</source>
        <translation>PS Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Pressure Support Minimum</source>
        <translation>Minimalne wsparcie ciśnieniem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>PS Max</source>
        <translation>PS Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Pressure Support Maximum</source>
        <translation>Maksymalne wsparcie ciśnieniem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Min Pressure</source>
        <translation>Ciśnienie min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Minimum Therapy Pressure</source>
        <translation>Minimalne ciśnienie lecznicze</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Max Pressure</source>
        <translation>Ciśnienie max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Maximum Therapy Pressure</source>
        <translation>Maksymalne ciśnienie lecznicze</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Ramp Time</source>
        <translation>Czas rozbiegu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Ramp Delay Period</source>
        <translation>Czas opóźnienia rozbiegu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Ramp Pressure</source>
        <translation>Ciśnienie rozbiegu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="148"/>
        <source>Starting Ramp Pressure</source>
        <translation>Początkowe ciśnienie rozbiegu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Ramp Event</source>
        <translation>Zdarzenie rozbiegu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="209"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1041"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Ramp</source>
        <translation>Rozbieg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>Chrapanie z wibracją (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Mask On Time</source>
        <translation>Czas z maską</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>Time started according to str.edf</source>
        <translation>Czas rozpoczęcia odpowiednio do str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>Summary Only</source>
        <translation>Tylko podsumowanie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>An apnea where the airway is open</source>
        <translation>Bezdech przy otwartych drogach oddechowych</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>Cheyne Stokes Respiration (CSR)</source>
        <translation>Oddech Cheyne-Stokes&apos;a (CSR)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>Periodic Breathing (PB)</source>
        <translation>Oddychanie okresowe (PB)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="160"/>
        <source>Clear Airway (CA)</source>
        <translation>Centralne (CA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>Bezdech spowodowany obturacją dróg oddechowych</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>A partially obstructed airway</source>
        <translation>Częściowo zamknięte drogi oddechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>UA</source>
        <translation>UA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>A vibratory snore</source>
        <translation>Chrapanie z wibracją</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2830"/>
        <source>Pressure Pulse</source>
        <translation>Impuls ciśnienia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2831"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>Impuls ciśnienia wysłany w celu wykrycia zamkniętych dróg oddechowych.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>Rodzaj zdarzenia oddechowego nie odpowiadającego na zwiększenie ciśnienia.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Zdarzenie intellipap gdy wydychasz przez usta.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>Funkcja SensAwake zredukuje ciśnienie gdy wykryje przebudzenie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Heart rate in beats per minute</source>
        <translation>Częstość akcji serca w uderzeniach na minutę</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>Wartość procentowa saturacji krwi tlenem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>Plethysomogram</source>
        <translation>Pletyzmogram</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="211"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>Optyczny foto-pletyzmogram pokazujący rytm serca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>Nagła (zdefiniowana przez użytkownika) zmiana częstosci akcji serca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>Nagła (zdefiniowana przez użytkownika) zmiana saturacji krwi tlenem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Breathing flow rate waveform</source>
        <translation>Wykres współczynnika przepływu oddechowego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Mask Pressure</source>
        <translation>Ciśnienie maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Amount of air displaced per breath</source>
        <translation>Ilość powietrza przemieszczonego na oddech</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Graph displaying snore volume</source>
        <translation>wykres ukazujący wielkość chrapania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Minute Ventilation</source>
        <translation>Wentylacja minutowa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="239"/>
        <source>Amount of air displaced per minute</source>
        <translation>Ilość powietrza przemieszczonego na minutę</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Respiratory Rate</source>
        <translation>Częstość oddechowa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="242"/>
        <source>Rate of breaths per minute</source>
        <translation>Częstość oddechów na minutę</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Patient Triggered Breaths</source>
        <translation>Oddechy wywołane przez pacjenta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Odsetek oddechów wywołanych przez pacjenta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="245"/>
        <source>Pat. Trig. Breaths</source>
        <translation>Pat. Trig. Breaths</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="248"/>
        <source>Leak Rate</source>
        <translation>Wskaźnik wycieków</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="248"/>
        <source>Rate of detected mask leakage</source>
        <translation>Wskaźnik wykrytych wycieków maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>I:E Ratio</source>
        <translation>I:E Ratio</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Stosunek czasu wdechu/wydechu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="709"/>
        <source>ratio</source>
        <translation>stosunek</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Pressure Min</source>
        <translation>Ciśnienie Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Pressure Max</source>
        <translation>Ciśnienie Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>CSR</source>
        <translation>CSR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="156"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>Nienormalny okres oddechów Cheyne-Stokes&apos;a</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="158"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>Nienormalny czas oddychania okresowego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="176"/>
        <source>A vibratory snore as detected by a System One device</source>
        <translation>Chrapanie wibracyjne wykryte przez urządzenie System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>Definiowane przez użytkownika zdarzenie wykryte przez OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>Perfusion Index</source>
        <translation>Indeks perfuzji</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>Względna osena siły pulsu na stanowisku monitorowania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="214"/>
        <source>Perf. Index %</source>
        <translation>Perf. Index %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Expiratory Time</source>
        <translation>Czas wydechu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Time taken to breathe out</source>
        <translation>Czas wydychania powietrza</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Inspiratory Time</source>
        <translation>Czas wdechu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Time taken to breathe in</source>
        <translation>Czas wdychania powietrza</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Respiratory Event</source>
        <translation>Zdarzenie oddechowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>Wykres pokazujący ciężkość ograniczeń przepływu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Flow Limit.</source>
        <translation>Ograniczenie przepływu.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Target Minute Ventilation</source>
        <translation>Docelowa wentylacja minutowa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Maximum Leak</source>
        <translation>Wyciek maksymalny</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>The maximum rate of mask leakage</source>
        <translation>Maksymalny wskaźnik wycieku maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Max Leaks</source>
        <translation>Wycieki max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>Wykres ukazujący zmiany AHI dla ostatniej godziny</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Total Leak Rate</source>
        <translation>Wskaźnik wycieku całkowitego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="276"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Wykryty wyciek maski z naturalnym wyciekiem maski włącznie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median Leak Rate</source>
        <translation>Mediana wskaźnika wycieku maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median rate of detected mask leakage</source>
        <translation>Mediana wskaźnika wykrytego wycieku maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="279"/>
        <source>Median Leaks</source>
        <translation>Mediana wycieków</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Wykres pokazujący bieżące RDI dla ostatniej godziny</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="286"/>
        <source>Sleep position in degrees</source>
        <translation>Pozycja snu w stopniach</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="289"/>
        <source>Upright angle in degrees</source>
        <translation>Kąt pochylenia w stopniach</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="298"/>
        <source>CPAP Session contains summary data only</source>
        <translation>Sesje CPAP zawierające tylko dane sumaryczne</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="775"/>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="776"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2837"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>PAP Mode</source>
        <translation>Tryb PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="162"/>
        <source>Obstructive Apnea (OA)</source>
        <translation>Bezdech obturacyjny (OA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Hypopnea (H)</source>
        <translation>Spłycony oddech (H)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>Unclassified Apnea (UA)</source>
        <translation>Bezdech niesklasyfikowany (UA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Apnea (A)</source>
        <translation>bezdech (a)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>An apnea reportred by your CPAP device.</source>
        <translation>Bezdech zaraportowany przez aparat.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>Flow Limitation (FL)</source>
        <translation>Ograniczenie przepływu (FL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>RERA (RE)</source>
        <translation>RERA (RE)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="172"/>
        <source>Respiratory Effort Related Arousal: A restriction in breathing that causes either awakening or sleep disturbance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Vibratory Snore (VS)</source>
        <translation>Chrapanie z wibracją (VS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <source>Leak Flag (LF)</source>
        <translation>Flaga wycieku (LF)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="179"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>A large mask leak affecting device performance.</source>
        <translation>Duży wyciek wpływający na efektywność działania aparatu.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>Large Leak (LL)</source>
        <translation>Duży wyciek (LL)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>Non Responding Event (NR)</source>
        <translation>Zdarzenie nie odpowiadające (NR)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="189"/>
        <source>Expiratory Puff (EP)</source>
        <translation>Pufnięcie wydechowe (EP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="192"/>
        <source>SensAwake (SA)</source>
        <translation>Przebudzenie sensoryczne (SA)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>User Flag #1 (UF1)</source>
        <translation>Flaga użytkownika #1 (UF1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="198"/>
        <source>User Flag #2 (UF2)</source>
        <translation>Flaga użytkownika #2 (UF2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="201"/>
        <source>User Flag #3 (UF3)</source>
        <translation>Flaga użytkownika #3(UF3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="217"/>
        <source>Pulse Change (PC)</source>
        <translation>Zmiany pulsu (PC)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="220"/>
        <source>SpO2 Drop (SD)</source>
        <translation>Spadek SpO2 (SD)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="273"/>
        <source>Apnea Hypopnea Index (AHI)</source>
        <translation>Indeks bezdechów i spłycenia oddechu (AHI)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="282"/>
        <source>Respiratory Disturbance Index (RDI)</source>
        <translation>Wskaźnik zaburzeń oddychania (RDI)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="301"/>
        <source>PAP Device Mode</source>
        <translation>Tryb urządzenia PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>APAP (Variable)</source>
        <translation>APAP (Variable)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (Fixed EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (Variable EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Height</source>
        <translation>Wzrost</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="318"/>
        <source>Physical Height</source>
        <translation>Wzrost fizyczny</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Notes</source>
        <translation>Notatki</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Bookmark Notes</source>
        <translation>Notatki zakładki</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Body Mass Index</source>
        <translation>Wskaźnik masy ciała</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Jak się czujesz (0-gówniano; 10-zajefajnie)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Bookmark Start</source>
        <translation>Początek zakładki</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Bookmark End</source>
        <translation>Koniec zakładki</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Last Updated</source>
        <translation>Ostatnio uaktualnione</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Journal Notes</source>
        <translation>Notatki dziennika</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Journal</source>
        <translation>Dziennik</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="333"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=obudzony, 2=REM, 3=lekki sen, 4=głęboki sen</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Brain Wave</source>
        <translation>Fale mózgowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>BrainWave</source>
        <translation>Fale mózgowe</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="336"/>
        <source>Awakenings</source>
        <translation>Przebudzenia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="336"/>
        <source>Number of Awakenings</source>
        <translation>Ilość przebudzeń</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>Morning Feel</source>
        <translation>Samopoczucie o poranku</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>How you felt in the morning</source>
        <translation>Jak się czułeś rano</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Time Awake</source>
        <translation>Czas przebudzenia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Time spent awake</source>
        <translation>Czas spędzony na przebudzeniu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time In REM Sleep</source>
        <translation>Czas snu w fazie REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time spent in REM Sleep</source>
        <translation>Czas snu w fazie REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Time in REM Sleep</source>
        <translation>Czas snu w fazie REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time In Light Sleep</source>
        <translation>Czas snu lekkiego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time spent in light sleep</source>
        <translation>Czas snu lekkiego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time in Light Sleep</source>
        <translation>Czas snu lekkiego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time In Deep Sleep</source>
        <translation>Czas snu głębokiego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time spent in deep sleep</source>
        <translation>Czas snu głębokiego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time in Deep Sleep</source>
        <translation>Czas snu głębokiego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time to Sleep</source>
        <translation>Czas zasypiania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time taken to get to sleep</source>
        <translation>Czas poświęcony na zaśnięcie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Zeo ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Pomiar jakości snu Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>ZEO ZQ</source>
        <translation>ZEO ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="779"/>
        <source>Zero</source>
        <translation>Zero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="782"/>
        <source>Upper Threshold</source>
        <translation>Górny próg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="785"/>
        <source>Lower Threshold</source>
        <translation>Dolny próg</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="525"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>Ponieważ nie wybrałeś foldera danych, OSCAR ucieka.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="537"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>Wybrany folder nie jest pusty ani nie zawiera prawidłowych danych OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="672"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>Możliwe, że zrobienie tego spowoduje utratę danych, jesteś pewny, że chcesz to zrobić?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <source>Question</source>
        <translation>Pytanie</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="524"/>
        <location filename="../oscar/main.cpp" line="576"/>
        <location filename="../oscar/main.cpp" line="591"/>
        <source>Exiting</source>
        <translation>Wychodzenie</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="538"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>Jesteś pewny, że chcesz użyć tego folderu?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="304"/>
        <source>OSCAR Reminder</source>
        <translation>OSCAR przypomina</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="304"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP device</source>
        <translation>Nie zapomnij włożyć swojej karty SD ponownie do aparatu CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="488"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>Możesz pracować tylko z jedną instancją pojedynczego profilu OSCAR naraz.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="489"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>Jeśli korzystasz z pamięci w chmurze, upewnij się, że OSCAR jest zamknięty, a synchronizacja zakończyła się przed kontynuowaniem.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="502"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>ładuję profil &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1209"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>Jesteś pewny, że chcesz przywrócić domyślne kolory i ustawienia?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1219"/>
        <source>Are you sure you want to reset all your oximetry settings to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1298"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>Jesteś pewny, że chcesz przywrócić domyślne kolory i ustawienia wykresów?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="45"/>
        <source>There are no graphs visible to print</source>
        <translation>Nie ma widocznych wykresów do wydruku</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="60"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>Chciałbyś pokazać obszary zakładek w tym raporcie?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="104"/>
        <source>Printing %1 Report</source>
        <translation>Drukuję raport %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="137"/>
        <source>%1 Report</source>
        <translation>Raport %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="195"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 godzin, %2 minut, %3 sekund
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="248"/>
        <source>RDI	%1
</source>
        <translation>RDI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="250"/>
        <source>AHI	%1
</source>
        <translation>AHI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="283"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI=%1 HI=%2 CAI=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="293"/>
        <source>UAI=%1 </source>
        <translation>UAI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="295"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI=%1 LKI=%2 EPI=%3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="366"/>
        <source>Reporting from %1 to %2</source>
        <translation>Raportowanie od %1 do %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="431"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Wykres przepływu całego dnia</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="433"/>
        <source>Current Selection</source>
        <translation>Bieżący wybór</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="443"/>
        <source>Entire Day</source>
        <translation>Cały dzień</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="568"/>
        <source>Page %1 of %2</source>
        <translation>Strony %1 z %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1009"/>
        <source>Days: %1</source>
        <translation>Dni: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1013"/>
        <source>Low Usage Days: %1</source>
        <translation>Dni niskiego użycia: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1014"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% zgodne, zdefiniowane jako &gt; %2 godzin)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1131"/>
        <source>(Sess: %1)</source>
        <translation>(Sesja:%1)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1139"/>
        <source>Bedtime: %1</source>
        <translation>Czas w łóżku:%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1141"/>
        <source>Waketime: %1</source>
        <translation>Czas budzenia: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gOverviewGraph.cpp" line="1254"/>
        <source>(Summary Only)</source>
        <translation>(Tylko podsumowanie)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="487"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Jest plik blokady dla tego profilu &apos;%1&apos;, zgłoszony na &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="306"/>
        <source>Fixed Bi-Level</source>
        <translation>Fixed Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Auto Bi-Level (Fixed PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Auto Bi-Level (Variable PS)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1588"/>
        <source>Fixed %1 (%2)</source>
        <translation>Fixed %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1591"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Min %1 Max %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1595"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP %1 IPAP %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1599"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS %1 ponad %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1604"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1613"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1630"/>
        <source>EPAP %1-%2 IPAP %3-%4 (%5)</source>
        <translation>EPAP %1-%2 IPAP %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="360"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Najświeższe dane pulsoksymetru: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="361"/>
        <source>(last night)</source>
        <translation>(ostatnia noc)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="368"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>Nie zaimportowano dotąd żadnych danych pulsoksymetrii.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="41"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="41"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="86"/>
        <source>SmartFlex Settings</source>
        <translation>Ustawienia SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="41"/>
        <source>ChoiceMMed</source>
        <translation>ChoiceMMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="41"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="70"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="70"/>
        <source>M-Series</source>
        <translation>M-Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="98"/>
        <source>EPR: </source>
        <translation>EPR: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose Software</source>
        <translation>Oprogramowanie Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Personal Sleep Coach</source>
        <translation>Osobisty trener snu</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="206"/>
        <source>Selection Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="249"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>Baza danych przeterminowana
Proszę przebuduj dane CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="430"/>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 min, %3 sec)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="432"/>
        <source> (%3 sec)</source>
        <translation> (%3 sek)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="552"/>
        <source>Pop out Graph</source>
        <translation>Wyskakujący wykres</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1621"/>
        <source>Your machine doesn&apos;t record data to graph in Daily View</source>
        <translation>Twoje urządzenie nie rejestruje danych na wykresie w widoku dziennym</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1624"/>
        <source>There is no data to graph</source>
        <translation>Nie ma danych dla wykresu</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2435"/>
        <source>Hide All Events</source>
        <translation>Ukryj wszystkie zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2436"/>
        <source>Show All Events</source>
        <translation>Pokaż wszystkie zdarzenia</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2777"/>
        <source>Unpin %1 Graph</source>
        <translation>Odepnij wykres %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2779"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2854"/>
        <source>Popout %1 Graph</source>
        <translation>Wydobądź wykres %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2856"/>
        <source>Pin %1 Graph</source>
        <translation>Przypnij wykres %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="809"/>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1034"/>
        <source>Plots Disabled</source>
        <translation>Wykresy wyłączone</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1117"/>
        <source>Duration %1:%2:%3</source>
        <translation>Czas trwania %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1118"/>
        <source>AHI %1</source>
        <translation>AHI %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="155"/>
        <source>Relief: %1</source>
        <translation>Ulga: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="161"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Godziny: %1h, %2m, %3s</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="264"/>
        <source>Machine Information</source>
        <translation>Informacje o aparacie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="29"/>
        <source>Journal Data</source>
        <translation>Dane dziennika</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="47"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR znalazł stary folder dziennika, ale wygląda na to, że został przemianowany:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="49"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>OSCAR nie dotknie tego folderu i zamiast tego utworzy nowy.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="50"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>Zachowaj ostrożność podczas zabawy w folderach profilu OSCAR :-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="57"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>Z jakiegoś powodu OSCAR nie mógł znaleźć zapisu dziennika w swoim profilu, ale znalazł wiele folderów danych dziennika.

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="58"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCAR wybrał tylko pierwszy z nich i użyje go w przyszłości:

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="60"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Jeśli brakuje danych, skopiuj zawartość wszystkich innych folderów Journal_XXXXXXX do tego tu ręcznie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="43"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2786"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2788"/>
        <source>SmartFlex Mode</source>
        <translation>Tryb SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2787"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Tryb ulgi ciśnieniowej Intellipap.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2793"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="135"/>
        <source>Ramp Only</source>
        <translation>Tylko rozbieg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="136"/>
        <source>Full Time</source>
        <translation>Całkowity czas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2797"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2799"/>
        <source>SmartFlex Level</source>
        <translation>Poziom SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2798"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Poziom ulgi ciśnieniowej Intellipap.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="547"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>Lokaizowanie pliku(ów) STR.edf ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="708"/>
        <source>Cataloguing EDF Files...</source>
        <translation>Katalogowanie plików EDF...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="727"/>
        <source>Queueing Import Tasks...</source>
        <translation>Kolejkowanie zadań importu ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="756"/>
        <source>Finishing Up...</source>
        <translation>Kończenie ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="113"/>
        <source>CPAP Mode</source>
        <translation>Tryb CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="123"/>
        <source>VPAPauto</source>
        <translation>VPAPauto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="125"/>
        <source>ASVAuto</source>
        <translation>ASVAuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="127"/>
        <source>PAC</source>
        <translation>PAC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="128"/>
        <source>Auto for Her</source>
        <translation>Auto for Her</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="132"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1030"/>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="132"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>ResMed ulga wydechowa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="137"/>
        <source>Patient???</source>
        <translation>Pacjent ???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="140"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1036"/>
        <source>EPR Level</source>
        <translation>Poziom EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="140"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>Poziom ulgi wydechowej</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>SmartStart</source>
        <translation>SmartStart</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="455"/>
        <source>Your ResMed CPAP device (Model %1) has not been tested yet.</source>
        <translation>Twój aparat ResMed (model %1) nie był dotąd testowany.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="456"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card to make sure it works with OSCAR.</source>
        <translation>Wygląda to dość podobnie do innych aparatów by mogło działać, ale deweloperzy chcieliby kopię (.zip) karty SD z tego aparatu by upewnić się, że działa w OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Smart Start</source>
        <translation>Smart Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humid. Status</source>
        <translation>Status nawilżacza</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="153"/>
        <source>Humidifier Enabled Status</source>
        <translation>Status włączenia nawilżacza</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2934"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="159"/>
        <source>Humid. Level</source>
        <translation>Poziom nawilżacza</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="159"/>
        <source>Humidity Level</source>
        <translation>Poziom wilgotności</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="172"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="172"/>
        <source>ClimateLine Temperature</source>
        <translation>Temperatura  ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Temp. Enable</source>
        <translation>Włącz. Temp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>Temperatura ClimateLine włączona</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="176"/>
        <source>Temperature Enable</source>
        <translation>Włączenie temperatury</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="183"/>
        <source>AB Filter</source>
        <translation>Filtr AB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="183"/>
        <source>Antibacterial Filter</source>
        <translation>Filtr antybakteryjny</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>Pt. Access</source>
        <translation>Dostęp Pt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="195"/>
        <source>Climate Control</source>
        <translation>Kontrola klimatu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="198"/>
        <source>Manual</source>
        <translation>Podręcznik</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="876"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2945"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3036"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="201"/>
        <source>Mask</source>
        <translation>Maska</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="201"/>
        <source>ResMed Mask Setting</source>
        <translation>Ustawienia maski ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="203"/>
        <source>Pillows</source>
        <translation>Poduszeczki</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="204"/>
        <source>Full Face</source>
        <translation>Pełnotwarzowa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="205"/>
        <source>Nasal</source>
        <translation>Nosowa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="209"/>
        <source>Ramp Enable</source>
        <translation>Włącz rozbieg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="118"/>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="118"/>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="430"/>
        <source>Snapshot %1</source>
        <translation>Migawka %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="280"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="280"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="695"/>
        <source>Loading %1 data for %2...</source>
        <translation>Ładuję %1 dane dla %2...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="706"/>
        <source>Scanning Files</source>
        <translation>Skanuję pliki</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="740"/>
        <source>Migrating Summary File Location</source>
        <translation>Przenoszenie lokalizacji plików podsumowań</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="910"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>Ładowanie Summaries.xml.gz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1042"/>
        <source>Loading Summary Data</source>
        <translation>Ładowanie danych podsumowań</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="15"/>
        <source>Please Wait...</source>
        <translation>Zaczekaj ...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>Using </source>
        <translation>Używam </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>, found SleepyHead -
</source>
        <translation>, znaleziono SleepyHead -Head -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="203"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>Musisz uruchomić narzędzie migracji OSCAR&apos;a</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="166"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>Bezdech, który nie moze być określony jako centralny czy obturacyjny.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="170"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>Ograniczenie oddechu, powodujące spłaszczenie wykresu przepływu.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="194"/>
        <source>or CANCEL to skip migration.</source>
        <translation>lub Skasuj aby pominąć przenoszenie.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="209"/>
        <source>You cannot use this folder:</source>
        <translation>Nie możesz użyć tego folderu:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="521"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>Wybierz albo utwórz nowy folder na dane OSCARa</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source>Migrating </source>
        <translation>Przenoszę </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source> files</source>
        <translation> pliki</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="225"/>
        <source>from </source>
        <translation>od </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="225"/>
        <source>to </source>
        <translation>do </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="511"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR przygotuje folder na Twoje dane.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="514"/>
        <source>We suggest you use this folder: </source>
        <translation>Proponujemy użycie tego folderu: </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="515"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>Kliknij OK aby zaakceptować, albo Nie jeśli chcesz użyć innego folderu.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="526"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>NGdy następny raz uruchomisz OSCARa, będziesz zapytany ponownie.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="221"/>
        <source>App key:</source>
        <translation>Klucz aplikacji:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="209"/>
        <source>Operating system:</source>
        <translation>System operacyjny:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="210"/>
        <source>Graphics Engine:</source>
        <translation>Silnik graficzny:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="211"/>
        <source>Graphics Engine type:</source>
        <translation>Typ silnika graficznego:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="558"/>
        <source>Data directory:</source>
        <translation>Folder danych:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="168"/>
        <source>Updating Statistics cache</source>
        <translation>Uaktualnienie cache statystyk</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="715"/>
        <source>Usage Statistics</source>
        <translation>Statystyki użycia</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1771"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation>d MMM yyyy [ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1609"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation>EPAP %1 PS %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Pressure Set</source>
        <translation>Ustawienie ciśnienia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Pressure Setting</source>
        <translation>Ustawianie ciśnienia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>IPAP Set</source>
        <translation>Ustawienie ciśnienia wdechowego (IPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>IPAP Setting</source>
        <translation>Ustawianie ciśnienia wdechowego (IPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>EPAP Set</source>
        <translation>Ustawienie ciśnienia wydechowego (EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>EPAP Setting</source>
        <translation>Ustawianie ciśnienia wydechowego (EPAP)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="752"/>
        <source>Loading summaries</source>
        <translation>Ładowanie podsumowań</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="207"/>
        <source>Built with Qt %1 on %2</source>
        <translation>Zbudowane z użyciem Qt %1 na %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="847"/>
        <source>Device</source>
        <translation>Urządzenie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="853"/>
        <source>Motion</source>
        <translation>Ruch</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1571"/>
        <source>n/a</source>
        <translation>n/a</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="37"/>
        <source>Dreem</source>
        <translation>Dreem</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="153"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="92"/>
        <source>Untested Data</source>
        <translation>Niesprawdzone dane</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2862"/>
        <source>P-Flex</source>
        <translation>P-flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2912"/>
        <source>Humidification Mode</source>
        <translation>Tryb nawilżania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2913"/>
        <source>PRS1 Humidification Mode</source>
        <translation>Tryb nawilżania aparatu PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2914"/>
        <source>Humid. Mode</source>
        <translation>Tryb nawilżania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2916"/>
        <source>Fixed (Classic)</source>
        <translation>Stały (klasyczny)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2917"/>
        <source>Adaptive (System One)</source>
        <translation>Adaptacyjny (System One)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2918"/>
        <source>Heated Tube</source>
        <translation>Podgrzewana rura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2924"/>
        <source>Tube Temperature</source>
        <translation>Temperatura rury</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2925"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation>Temperatura podgrzewanej rury PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2926"/>
        <source>Tube Temp.</source>
        <translation>Temp. rury.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2933"/>
        <source>PRS1 Humidifier Setting</source>
        <translation>Ustawienie nawilżania PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2963"/>
        <source>12mm</source>
        <translation>12mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="93"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation>Twój aparat Viatom wygenerował dane, których OSCAR jeszcze nie widział.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="94"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation>Dane zaimportowanie mogą nie być dokładnem dlatego deweloperzy chcieliby kopię Twoich plików Viatom, dla pewności, ze OSCAR prawidłowo obsługuje dane.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom</source>
        <translation>Viatom</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom Software</source>
        <translation>Oprogramowanie Viatom</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="538"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation>OSCAR %1 potrzebuje uaktualnić bazę danych dla %2 %3 %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>Movement</source>
        <translation>Ruch</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>Movement detector</source>
        <translation>Detektor ruchu</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="659"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation>Wersja :%1&quot; jest nieprawidłowa, nie można kontynuować!</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="668"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation>Wersja której używasz (%1) jest starsza od tej, w której utworzono te dane (%2).</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2666"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation>Proszę wybierz miejsce zapisu archiwum inne niż karta z danymi!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2713"/>
        <location filename="../oscar/mainwindow.cpp" line="2763"/>
        <location filename="../oscar/mainwindow.cpp" line="2822"/>
        <source>Unable to create zip!</source>
        <translation>Nie mogę utworzyć archiwum!</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1225"/>
        <source>Parsing STR.edf records...</source>
        <translation>Analiza zapisów STR.edf...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="526"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="888"/>
        <source>Backing Up Files...</source>
        <translation>Zapisywanie kopii zapasowej plików...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Mask Pressure (High frequency)</source>
        <translation>Ciśnienie w masce (Wysoka częstotliwość)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation>Element danych ResMed: zdarzenie wyzwalania  cyklu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <source>Debugging channel #1</source>
        <translation>Kanał debugowania #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <source>Test #1</source>
        <translation>Test #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>Debugging channel #2</source>
        <translation>Debugowanie kanał #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>Test #2</source>
        <translation>Test #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1624"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation>EPAP %1 IPAP %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2840"/>
        <source>CPAP-Check</source>
        <translation>CPAP-Check</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2842"/>
        <source>AutoCPAP</source>
        <translation>AutoCPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2843"/>
        <source>Auto-Trial</source>
        <translation>Auto-Trial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2845"/>
        <source>AutoBiLevel</source>
        <translation>AutoBiLevel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2847"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2848"/>
        <source>S/T</source>
        <translation>S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2850"/>
        <source>S/T - AVAPS</source>
        <translation>S/T - AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2851"/>
        <source>PC - AVAPS</source>
        <translation>PC - AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2878"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2880"/>
        <source>Flex Lock</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2879"/>
        <source>Whether Flex settings are available to you.</source>
        <translation>Czy są dostępne ustawienia Flex.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2888"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation>Ilość czasu zmiany EPAP do IPAP, im wyższy tym wolniejsza zmiana</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2894"/>
        <source>Rise Time Lock</source>
        <translation>Blokada czasu podwyższania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2895"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation>Czy ustawienia czasu wzrostu są dostępne.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2896"/>
        <source>Rise Lock</source>
        <translation>Blokada podwyższania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2949"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2950"/>
        <source>Mask Resistance Setting</source>
        <translation>Ustawienie oporu maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2951"/>
        <source>Mask Resist.</source>
        <translation>Opór maski.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2959"/>
        <source>Hose Diam.</source>
        <translation>Średnica węża.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2967"/>
        <source>Tubing Type Lock</source>
        <translation>Blokada typu rury</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2968"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation>Czy są dostępne ustawienia rodzaju rury.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2969"/>
        <source>Tube Lock</source>
        <translation>Blokada rury</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2976"/>
        <source>Mask Resistance Lock</source>
        <translation>Blokada oporu maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2977"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation>Czy są dostępne ustawienia oporu maski.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2978"/>
        <source>Mask Res. Lock</source>
        <translation>Blokada oporu maski</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3021"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3023"/>
        <source>Ramp Type</source>
        <translation>Typ rampy</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3022"/>
        <source>Type of ramp curve to use.</source>
        <translation>Rodzaj krzywej ramp do użycia.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3025"/>
        <source>Linear</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3026"/>
        <source>SmartRamp</source>
        <translation>SmartRamp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3027"/>
        <source>Ramp+</source>
        <translation>Ramp+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3031"/>
        <source>Backup Breath Mode</source>
        <translation>Tryb oddechu zastępczego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3032"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation>Rodzaj oddechu zastępczego w użyciu żaden (off) automatyczny, ustalony</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3033"/>
        <source>Breath Rate</source>
        <translation>Częstość oddechów</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3037"/>
        <source>Fixed</source>
        <translation>Ustalone</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3041"/>
        <source>Fixed Backup Breath BPM</source>
        <translation>Ustalony oddech zastępczy BPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3042"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation>Minimalne oddechy na minutę (BPM) poniżej których inicjowany jest oddech</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3043"/>
        <source>Breath BPM</source>
        <translation>Oddechy BPM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3048"/>
        <source>Timed Inspiration</source>
        <translation>Czasowy wdech</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3049"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation>Czas gdy ustalony wdech będzie dostarczał IPAP przed przejściem w EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3050"/>
        <source>Timed Insp.</source>
        <translation>Czasowy wdech.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3055"/>
        <source>Auto-Trial Duration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3057"/>
        <source>Auto-Trial Dur.</source>
        <translation>Czas trwania Auto-Trial.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3062"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3064"/>
        <source>EZ-Start</source>
        <translation>EZ-Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3063"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation>Czy jest włączony tryb EZ-Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3071"/>
        <source>Variable Breathing</source>
        <translation>Oddychanie zmienne</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3072"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation>NIEPOTWIERDZONE: Prawdopodobnie zmienne oddychanie, z okresami o wysokim odchyleniu od szczytowego trendu wdechowego</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="559"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>Jak już zaktualizujesz &lt;font size=+1&gt;nie możesz&lt;/font&gt; już używać tego profilu z poprzednią wersją.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2919"/>
        <source>Passover</source>
        <translation>Przepuszczenie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2986"/>
        <source>A few breaths automatically starts device</source>
        <translation>Kilka oddechów automatycznie włącza aparat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2995"/>
        <source>Device automatically switches off</source>
        <translation>Aparat wyłącza się automatycznie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3004"/>
        <source>Whether or not device allows Mask checking.</source>
        <translation>Czy aparat pokazuje alarm maski.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3013"/>
        <source>Whether or not device shows AHI via built-in display.</source>
        <translation>Czy aparat pokazuje AHI.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3056"/>
        <source>The number of days in the Auto-CPAP trial period, after which the device will revert to CPAP</source>
        <translation>Ilość dni w okresie próbnym auto-CPAP, po którym aparat wróci do trybu CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3081"/>
        <source>A period during a session where the device could not detect flow.</source>
        <translation>Przez pewien czas sesji aparat nie wykrył przepływu.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3095"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3097"/>
        <source>Peak Flow</source>
        <translation>Szczytowy przepływ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3096"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation>Przepływ szczytowy podczas 2-minutowego interwału</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2139"/>
        <source>Recompressing Session Files</source>
        <translation>Rekompresja plików sesji</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="343"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation>OSCAR wysypał si e z powodu niekompatybilności z grafiką Twojego komputera.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="344"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation>Aby to rozwiązać, OSCAR przeszedł na wolniejszą ale bardziej kompatybilną metodę rysowania.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="121"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="362"/>
        <source>(1 day ago)</source>
        <translation>(1 dzień wczeniej)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="363"/>
        <source>(%2 days ago)</source>
        <translation>(%2 dni wcześniej)</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="152"/>
        <source>New versions file improperly formed</source>
        <translation>Plik nowej wersji jest nieprawidłowo utworzony</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="175"/>
        <source>A more recent version of OSCAR is available</source>
        <translation>Dostępna jest nowsza wersja OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>release</source>
        <translation>wydanie</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>test version</source>
        <translation>wersja testowa</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="171"/>
        <source>You are running the latest %1 of OSCAR</source>
        <translation>Używasz najnowszego %1 OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="172"/>
        <location filename="../oscar/checkupdates.cpp" line="176"/>
        <source>You are running OSCAR %1</source>
        <translation>Używasz OSCAR %1</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="178"/>
        <source>OSCAR %1 is available &lt;a href=&apos;%2&apos;&gt;here&lt;/a&gt;.</source>
        <translation>OSCAR %1 jest dostępny &lt;a href=&apos;%2&apos;&gt;here&lt;/a&gt;.</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="181"/>
        <source>Information about more recent test version %1 is available at &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt;</source>
        <translation>Informacje o nowszej wersji testowej %1 są dostępne pod adresem &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="209"/>
        <source>Check for OSCAR Updates</source>
        <translation>Sprawdzanie uaktualnień OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="577"/>
        <source>Unable to create the OSCAR data folder at</source>
        <translation>Nie można utworzyć folderu danych OSCARa w</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="634"/>
        <source>The popout window is full. You should capture the existing
popout window, delete it, then pop out this graph again.</source>
        <translation>Okno wyskakujące jest pełne. Powinieneś uchwycić istniejące
wyskakujące okienko, usunąć je, a następnie otworzyć ponownie ten wykres.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>Essentials</source>
        <translation>Elementy zasadnicze</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="191"/>
        <source>Plus</source>
        <translation>Plus</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="587"/>
        <source>Unable to write to OSCAR data directory</source>
        <translation>Nie można zapisać w folderze danych OSCARa</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="588"/>
        <source>Error code</source>
        <translation>Kod błędu</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="589"/>
        <source>OSCAR cannot continue and is exiting.</source>
        <translation>OSCAR nie może kontynuować, wyłącza się.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="600"/>
        <source>Unable to write to debug log. You can still use the debug pane (Help/Troubleshooting/Show Debug Pane) but the debug log will not be written to disk.</source>
        <translation>Nie można zapisać w dzienniku debugowania. Nadal można używać okienka debugowania (Pomoc / Rozwiązywanie problemów / Pokaż okienko debugowania), ale dziennik debugowania nie zostanie zapisany na dysku.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="354"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="355"/>
        <source>For internal use only</source>
        <translation>Tylko do użytku wewnętrznego</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="193"/>
        <source>Choose the SleepyHead or OSCAR data folder to migrate</source>
        <translation>Wybierz folder danych SleepyHead lub OSCAR do migracji</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="208"/>
        <source>The folder you chose does not contain valid SleepyHead or OSCAR data.</source>
        <translation>Wybrany folder nie zawiera prawidłowych danych SleepyHead ani OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="512"/>
        <source>If you have been using SleepyHead or an older version of OSCAR,</source>
        <translation>Jeśli używasz SleepyHead lub starszej wersji OSCAR,</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="513"/>
        <source>OSCAR can copy your old data to this folder later.</source>
        <translation>OSCAR może później skopiować stare dane do tego folderu.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="566"/>
        <source>Migrate SleepyHead or OSCAR Data?</source>
        <translation>Migrować dane SleepyHead lub OSCAR?</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="567"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead or OSCAR data</source>
        <translation>Na następnym ekranie OSCAR poprosi Cię o wybranie folderu z danymi SleepyHead lub OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="568"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead or OSCAR data.</source>
        <translation>Kliknij [OK], aby przejść do następnego ekranu lub [Nie], jeśli nie chcesz używać żadnych danych SleepyHead lub OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="968"/>
        <source>Chromebook file system detected, but no removable device found
</source>
        <translation>Wykryto system plików Chromebooka, ale nie znaleziono urządzenia wymiennego
</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="969"/>
        <source>You must share your SD card with Linux using the ChromeOS Files program</source>
        <translation>Musisz udostępnić swoją kartę SD Linuksowi za pomocą programu Pliki systemu operacyjnego ChromeOS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2866"/>
        <source>Flex</source>
        <translation>Flex</translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="276"/>
        <source>Unable to check for updates. Please try again later.</source>
        <translation>Nie można sprawdzać aktualizacji. Proszę spróbować ponownie później.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2940"/>
        <source>Target Time</source>
        <translation>Czas docelowy</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2941"/>
        <source>PRS1 Humidifier Target Time</source>
        <translation>Czas docelowy nawilżacza PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2942"/>
        <source>Hum. Tgt Time</source>
        <translation>Czas docel. nawilżania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1537"/>
        <source>varies</source>
        <translation>różne</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2717"/>
        <source>Backing up files...</source>
        <translation>Zachowywanie plików...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2724"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="249"/>
        <source>Reading data files...</source>
        <translation>Odczytywanie plików danych...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2805"/>
        <source>Snoring event.</source>
        <translation>Zdarzenie chrapania.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2806"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="963"/>
        <source>model %1</source>
        <translation>model %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="966"/>
        <source>unknown model</source>
        <translation>nieznany model</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="126"/>
        <source>iVAPS</source>
        <translation>iVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="216"/>
        <source>Response</source>
        <translation>Odpowiedź</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="219"/>
        <source>Soft</source>
        <translation>Miękki</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.cpp" line="794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="218"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="119"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="122"/>
        <source>BiPAP-T</source>
        <translation>BiPAP-T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="120"/>
        <source>BiPAP-S</source>
        <translation>BiPAP-S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="121"/>
        <source>BiPAP-S/T</source>
        <translation>BiPAP-S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Device auto starts by breathing</source>
        <translation>Aparat sam startuje przez rozpoczęcie oddychania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>SmartStop</source>
        <translation>SmartStop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>Smart Stop</source>
        <translation>Smart Stop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="222"/>
        <source>Device auto stops by breathing</source>
        <translation>Aparat wyłącza się sam przez oddychanie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="228"/>
        <source>Patient View</source>
        <translation>Widok pacjenta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="231"/>
        <source>Simple</source>
        <translation>Prosty</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="230"/>
        <source>Advanced</source>
        <translation>Zaawansowany</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1022"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1023"/>
        <source>SensAwake level</source>
        <translation>poziom czułości wybudzania</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1030"/>
        <source>Expiratory Relief</source>
        <translation>ulga wydechowa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1036"/>
        <source>Expiratory Relief Level</source>
        <translation>poziom ulgi wydechowej</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1048"/>
        <source>Humidity</source>
        <translation>wilgotność</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>SleepStyle</source>
        <translation>styl snu</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="298"/>
        <source>AI=%1 </source>
        <translation>AI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="81"/>
        <source>This page in other languages:</source>
        <translation>Ta strona w innych językach:</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2643"/>
        <location filename="../oscar/overview.cpp" line="471"/>
        <source>%1 Graphs</source>
        <translation>%1 Wykresów</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2647"/>
        <location filename="../oscar/overview.cpp" line="475"/>
        <source>%1 of %2 Graphs</source>
        <translation>%1 z %2 Wykresów</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2674"/>
        <source>%1 Event Types</source>
        <translation>%1 Zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2678"/>
        <source>%1 of %2 Event Types</source>
        <translation>%1 z %2 Zdarzeń</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.h" line="223"/>
        <source>Löwenstein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prisma_loader.h" line="223"/>
        <source>Prisma Smart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveGraphLayoutSettings</name>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="179"/>
        <source>Manage Save Layout Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="186"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="187"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="186"/>
        <source>Add Feature inhibited. The maximum number of Items has been exceeded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="187"/>
        <source>creates new copy of current settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="188"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="188"/>
        <source>Restores saved settings from selection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="189"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="189"/>
        <source>Renames the selection. Must edit existing name then press enter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="190"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="190"/>
        <source>Updates the selection with current settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="191"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="191"/>
        <source>Deletes the selection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="192"/>
        <source>Expanded Help menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="193"/>
        <source>Exits the Layout menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="228"/>
        <source>&lt;h4&gt;Help Menu - Manage Layout Settings&lt;/h4&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="237"/>
        <source>Exits the help menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="238"/>
        <source>Exits the dialog menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="256"/>
        <source>     &lt;p style=&quot;color:black;&quot;&gt;        This feature manages the saving and restoring of Layout Settings.      &lt;br&gt;      Layout Settings control the layout of a graph or chart.      &lt;br&gt;      Different Layouts Settings can be saved and later restored.      &lt;br&gt;     &lt;/p&gt;     &lt;table width=&quot;100%&quot;&gt;         &lt;tr&gt;&lt;td&gt;&lt;b&gt;Button&lt;/b&gt;&lt;/td&gt;             &lt;td&gt;&lt;b&gt;Description&lt;/b&gt;&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Add&lt;/td&gt; 			&lt;td&gt;Creates a copy of the current Layout Settings. &lt;br&gt; 				The default description is the current date. &lt;br&gt; 				The description may be changed. &lt;br&gt; 				The Add button will be greyed out when maximum number is reached.&lt;/td&gt;&lt;/tr&gt;                 &lt;br&gt;         &lt;tr&gt;&lt;td&gt;&lt;i&gt;&lt;u&gt;Other Buttons&lt;/u&gt; &lt;/i&gt;&lt;/td&gt;              &lt;td&gt;Greyed out when there are no selections&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Restore&lt;/td&gt; 			&lt;td&gt;Loads the Layout Settings from the selection. Automatically exits. &lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Rename &lt;/td&gt;         			&lt;td&gt;Modify the description of the selection. Same as a double click.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Update&lt;/td&gt;&lt;td&gt; Saves the current Layout Settings to the selection.&lt;br&gt; 		        Prompts for confirmation.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td valign=&quot;top&quot;&gt;Delete&lt;/td&gt; 			&lt;td&gt;Deletes the selecton. &lt;br&gt; 			    Prompts for confirmation.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;&lt;i&gt;&lt;u&gt;Control&lt;/u&gt; &lt;/i&gt;&lt;/td&gt;              &lt;td&gt;&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Exit &lt;/td&gt; 			&lt;td&gt;(Red circle with a white &quot;X&quot;.) Returns to OSCAR menu.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Return&lt;/td&gt; 			&lt;td&gt;Next to Exit icon. Only in Help Menu. Returns to Layout menu.&lt;/td&gt;&lt;/tr&gt;         &lt;tr&gt;&lt;td&gt;Escape Key&lt;/td&gt; 			&lt;td&gt;Exit the Help or Layout menu.&lt;/td&gt;&lt;/tr&gt;       &lt;/table&gt;        &lt;p&gt;&lt;b&gt;Layout Settings&lt;/b&gt;&lt;/p&gt;       &lt;table width=&quot;100%&quot;&gt;          &lt;tr&gt; 			&lt;td&gt;* Name&lt;/td&gt; 			&lt;td&gt;* Pinning&lt;/td&gt; 			&lt;td&gt;* Plots Enabled &lt;/td&gt; 			&lt;td&gt;* Height&lt;/td&gt; 		&lt;/tr&gt;         &lt;tr&gt; 			&lt;td&gt;* Order&lt;/td&gt; 			&lt;td&gt;* Event Flags&lt;/td&gt; 			&lt;td&gt;* Dotted Lines&lt;/td&gt; 			&lt;td&gt;* Height Options&lt;/td&gt; 		&lt;/tr&gt;       &lt;/table&gt;        &lt;p&gt;&lt;b&gt;General Information&lt;/b&gt;&lt;/p&gt; 	  &lt;ul style=margin-left=&quot;20&quot;; &gt;  		&lt;li&gt; Maximum description size = 80 characters.	&lt;/li&gt;  		&lt;li&gt; Maximum Saved Layout Settings = 30.	&lt;/li&gt;  		&lt;li&gt; Saved Layout Settings can be accessed by all profiles.  		&lt;li&gt; Layout Settings only control the layout of a graph or chart. &lt;br&gt;               They do not contain any other data. &lt;br&gt;              They do not control if a graph is displayed or not. &lt;/li&gt; 		&lt;li&gt; Layout Settings for daily and overview are managed independantly. &lt;/li&gt;	  &lt;/ul&gt;   </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="458"/>
        <source>Maximum number of Items exceeded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="464"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="473"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="482"/>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="509"/>
        <source>No Item Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="465"/>
        <source>Ok to Update?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/saveGraphLayoutSettings.cpp" line="510"/>
        <source>Ok To Delete?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="247"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="290"/>
        <source>No Sessions Present</source>
        <translation>Brak sesji</translation>
    </message>
</context>
<context>
    <name>SleepStyleLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="213"/>
        <source>Import Error</source>
        <translation>Błąd importu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation>Ten zapis z aparatu nie moze być zaimportowany do tego profilu.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Zapis z dnia nakłada się na istniejący zapis.</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="536"/>
        <source>CPAP Statistics</source>
        <translation>Statystyki CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <location filename="../oscar/statistics.cpp" line="1400"/>
        <source>CPAP Usage</source>
        <translation>Użycie CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="540"/>
        <source>Average Hours per Night</source>
        <translation>Średnio godzin na noc</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="543"/>
        <source>Therapy Efficacy</source>
        <translation>Efektywność leczenia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="556"/>
        <source>Leak Statistics</source>
        <translation>Statystyka wycieków</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="561"/>
        <source>Pressure Statistics</source>
        <translation>Statystyka ciśnienia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="586"/>
        <source>Oximeter Statistics</source>
        <translation>Statystyki pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Saturacja krwi tlenem</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>Pulse Rate</source>
        <translation>Częstotliwość pulsu</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="606"/>
        <source>%1 Median</source>
        <translation>Mediana %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="607"/>
        <location filename="../oscar/statistics.cpp" line="608"/>
        <source>Average %1</source>
        <translation>Średnia %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="610"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="611"/>
        <source>Max %1</source>
        <translation>Max %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="612"/>
        <source>%1 Index</source>
        <translation>Wskaźnik %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="613"/>
        <source>% of time in %1</source>
        <translation>% czasu w %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="614"/>
        <source>% of time above %1 threshold</source>
        <translation>% czasu powyżej granicy %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="615"/>
        <source>% of time below %1 threshold</source>
        <translation>% czasu poniżej granicy %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="637"/>
        <source>Name: %1, %2</source>
        <translation>Nazwisko: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="639"/>
        <source>DOB: %1</source>
        <translation>Data urodzenia: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="642"/>
        <source>Phone: %1</source>
        <translation>Telefon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="645"/>
        <source>Email: %1</source>
        <translation>Email: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="648"/>
        <source>Address:</source>
        <translation>Adres:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="919"/>
        <source>Device Information</source>
        <translation>Informacje o aparacie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="978"/>
        <source>Changes to Device Settings</source>
        <translation>Zmiany ustawień urządzenia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1401"/>
        <source>Days Used: %1</source>
        <translation>Dni użycia: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1402"/>
        <source>Low Use Days: %1</source>
        <translation>Dni niskiego używania: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1403"/>
        <source>Compliance: %1%</source>
        <translation>Użycie zgodne z wymaganiami: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1427"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>Dni z AHI =5 lub powyżej: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1434"/>
        <source>Best AHI</source>
        <translation>Najlepsze AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1438"/>
        <location filename="../oscar/statistics.cpp" line="1450"/>
        <source>Date: %1 AHI: %2</source>
        <translation>Dnia: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1444"/>
        <source>Worst AHI</source>
        <translation>Najgorsze AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1481"/>
        <source>Best Flow Limitation</source>
        <translation>Najlepsze ograniczenia przepływu (FL)</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1485"/>
        <location filename="../oscar/statistics.cpp" line="1498"/>
        <source>Date: %1 FL: %2</source>
        <translation>Dnia: %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1491"/>
        <source>Worst Flow Limtation</source>
        <translation>Najgorsze ograniczenia przepływu</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1503"/>
        <source>No Flow Limitation on record</source>
        <translation>Brak ograniczeń przepływu w zapisie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1524"/>
        <source>Worst Large Leaks</source>
        <translation>Najgorsze duże wycieki</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1532"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>Dnia: %1 Wyciek: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1538"/>
        <source>No Large Leaks on record</source>
        <translation>Brak dużych wycieków w zapisie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1561"/>
        <source>Worst CSR</source>
        <translation>Najgorszy oddech okresowy (CSR)</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1569"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>Dnia: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1574"/>
        <source>No CSR on record</source>
        <translation>Brak oddechów okresowych w zapisie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1591"/>
        <source>Worst PB</source>
        <translation>Najgorszy oddech okresowy</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1599"/>
        <source>Date: %1 PB: %2%</source>
        <translation>Dnia: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1604"/>
        <source>No PB on record</source>
        <translation>Brak oddechów okresowych w zapisie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1612"/>
        <source>Want more information?</source>
        <translation>Chcesz więcej informacji?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1613"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR potrzebuje wszystkich danych podsumowań do obliczenia najlepszych/najgorszych danych dla poszczególnych dni.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1614"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>Proszę zaptaszkuj w Preferencjach &quot;załaduj podsumowania na starcie&quot;.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1634"/>
        <source>Best RX Setting</source>
        <translation>Najlepsze ustawienia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1637"/>
        <location filename="../oscar/statistics.cpp" line="1649"/>
        <source>Date: %1 - %2</source>
        <translation>Dnia: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1646"/>
        <source>Worst RX Setting</source>
        <translation>Najgorsze ustawienia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1166"/>
        <source>Most Recent</source>
        <translation>Najnowsze</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1167"/>
        <source>Last Week</source>
        <translation>Ostatni tydzień</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1168"/>
        <source>Last 30 Days</source>
        <translation>Ostatnie 30 dni</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1169"/>
        <source>Last 6 Months</source>
        <translation>Ostatnie 6 miesięcy</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1170"/>
        <source>Last Year</source>
        <translation>Ostatni rok</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1174"/>
        <source>Last Session</source>
        <translation>Ostatnia sesja</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1219"/>
        <source>Details</source>
        <translation>Szczegóły</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1233"/>
        <source>No %1 data available.</source>
        <translation>Brak danych %1.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1236"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 dzień z danymi %2 na %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1242"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 dni z danymi %2, między %3 a %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="986"/>
        <source>Days</source>
        <translation>Dni</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="990"/>
        <source>Pressure Relief</source>
        <translation>Ulga ciśnienia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="992"/>
        <source>Pressure Settings</source>
        <translation>Ustawienia ciśnienia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="925"/>
        <source>First Use</source>
        <translation>Pierwsze użycie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="926"/>
        <source>Last Use</source>
        <translation>Ostatnie użycie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="737"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR to wolne oprogramowanie open source do raportowania CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1066"/>
        <source>Oscar has no data to report :(</source>
        <translation>OSCAR nie ma danych do raportu :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation>Zgodność (%1 godz/dzień)</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1064"/>
        <source>No data found?!?</source>
        <translation>Nie znaleziono danych?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1640"/>
        <location filename="../oscar/statistics.cpp" line="1652"/>
        <source>AHI: %1</source>
        <translation>AHI: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1641"/>
        <location filename="../oscar/statistics.cpp" line="1653"/>
        <source>Total Hours: %1</source>
        <translation>Razem godzin: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="734"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation>Ten raport został przygotowany %1 przez OSCAR %2</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="142"/>
        <source>What would you like to do?</source>
        <translation>Co chcesz zrobić?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="185"/>
        <source>CPAP Importer</source>
        <translation>Import danych CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="223"/>
        <source>Oximetry Wizard</source>
        <translation>Kreator pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="261"/>
        <source>Daily View</source>
        <translation>Widok dzienny</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="299"/>
        <source>Overview</source>
        <translation>Przegląd</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="337"/>
        <source>Statistics</source>
        <translation>Statystyki</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="580"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap device.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;span style=&quot; font-weight:600;&quot;&gt;UWAGA: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;Karty SD z aparatów ResMed S9 muszą być zablokowane &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;przed włożeniem do komputera.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Niektóre systemy operacyjne dopisują pliki indeksu na karcie bez pytania, co może uczynić kartę nieczytelną dla aparatu CPAP.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>Dobrym pomysłem jest sprawdzenie najpierw - Plik-Preferencje</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation>ponieważ jest tam kilka opcji wpływających na import.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed device is detected</source>
        <translation>Zauważ, że kilka opcji jest wymuszonych po wykryciu aparatu ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>Pierwszy import trwa kilka minut.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>Ostatnio użyto aparatu %1...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>ostatniej nocy</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="183"/>
        <source>%2 days ago</source>
        <translation>%2 dni temu</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="185"/>
        <source>was %1 (on %2)</source>
        <translation>%1 ( %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="193"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 godz., %2 min. i %3 sek</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>Your device was on for %1.</source>
        <translation>Aparat działał przez %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="258"/>
        <source>Your CPAP device used a constant %1 %2 of air</source>
        <translation>Twój aparat podawał stałe ciśnienie %1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="271"/>
        <source>Your device used a constant %1-%2 %3 of air.</source>
        <translation>Ten aparat podawał stałe %1-%2 %3 powietrza.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="278"/>
        <source>Your device was under %1-%2 %3 for %4% of the time.</source>
        <translation>Aparat był poniżej %1-%2 %3 przez %4% czasu.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="197"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;Maska była założona przez %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>under</source>
        <translation>poniżej</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>over</source>
        <translation>powyżej</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="212"/>
        <source>reasonably close to</source>
        <translation>rozsądnie blisko</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="213"/>
        <source>equal to</source>
        <translation>równe z</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="227"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>AHI wynosiło %1, co stanowi %2 %3 dniowej średniej wynoszącej %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="318"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>Średnie wycieki wynosiły %1 %2, co stanowi %3  %4 dniowej średniej  %5.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="324"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>Dotąd nie zaimportowano żadnych danych CPAP.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="127"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Witaj w OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="263"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>Ciśnienie było poniżej %1 %2 przez %3% czasu.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="286"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>EPAP (ciśnienie na wydechu) ustaliło się na poziomie %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="289"/>
        <location filename="../oscar/welcome.cpp" line="298"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>IPAP (ciśnienie na wdechu) było poniżej %1 %2 przez %3% czasu.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="297"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>EPAP (ciśnienie na wydechu) było poniżej %1 %2 przez %3% czasu.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>1 day ago</source>
        <translation>1 dzień wcześniej</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="652"/>
        <source>Double click Y-axis: Return to AUTO-FIT Scaling</source>
        <translation>Kliknij dwukrotnie oś Y: aby powrócić do skalowania AUTO-FIT</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="654"/>
        <source>Double click Y-axis: Return to DEFAULT Scaling</source>
        <translation>Kliknij dwukrotnie oś Y: aby powrócićt do skalowania DOMYŚLNEGO</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="656"/>
        <source>Double click Y-axis: Return to OVERRIDE Scaling</source>
        <translation>Dwukrotne kliknięcie osi Y: aby powrócić do opcji ZMIEŃ skalowanie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="659"/>
        <source>Double click Y-axis: For Dynamic Scaling</source>
        <translation>Kliknij dwukrotnie oś Y: dla dynamicznego skalowania</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="663"/>
        <source>Double click Y-axis: Select DEFAULT Scaling</source>
        <translation>Kliknij dwukrotnie oś Y: Wybierz skalowanie DOMYŚLNE</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="665"/>
        <source>Double click Y-axis: Select AUTO-FIT Scaling</source>
        <translation>Kliknij dwukrotnie oś Y: Wybierz skalowanie AUTO-FIT</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="924"/>
        <source>%1 days</source>
        <translation>%1 dni</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="557"/>
        <source>100% zoom level</source>
        <translation>100% powiększenie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="563"/>
        <source>Reset Graph Layout</source>
        <translation>Resetuj Układ Wykresu</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="564"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Przywróć wszystkie wykresy do jednakowej wysokości i domyślnego układu.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="567"/>
        <source>Y-Axis</source>
        <translation>Oś Y</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="568"/>
        <source>Plots</source>
        <translation>Wykresy</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="573"/>
        <source>CPAP Overlays</source>
        <translation>Nakładki CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="576"/>
        <source>Oximeter Overlays</source>
        <translation>Nakładki pulsoksymetru</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="579"/>
        <source>Dotted Lines</source>
        <translation>Linie kropkowane</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2266"/>
        <source>Remove Clone</source>
        <translation>Usuń klona</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2270"/>
        <source>Clone %1 Graph</source>
        <translation>Wykres klona %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1967"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>Kliknij dwukrotnie apy przypiąć/odpiąć
Kliknij i przeciągnij aby zmienić układ wykresów</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="559"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation>Przywróć zoom osi X do 100% aby zobaczyć cały zaznaczony okres.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="561"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation>Przywróć zoom osi X do 100% aby zobaczyć dane całego dnia.</translation>
    </message>
</context>
</TS>
